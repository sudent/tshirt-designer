<?php
namespace Deployer;

require 'recipe/symfony4.php';

// Project name
set('application', 'tshirt-designer.syamsudin.dev');

// Project repository
set('repository', 'git@bitbucket.org:sudent/tshirt-designer.git');
set('branch', 'master');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);
set('ssh_multiplexing', true);

// Shared files/dirs between deploys 
add('shared_files', [
    '.env',
    'public/.htaccess',
    'public/.user.ini',
    'public/php.ini',
    'config/secrets/prod/prod.decrypt.private.php',
    'error.log'
]);
add('shared_dirs', [
    'public/images',
    'public/media',
    'public/uploads'
]);

// Writable dirs by web server 
add('writable_dirs', [
    'var/log',
    'var/cache',
    'public/images',
    'public/media',
    'public/uploads'
]);
set('allow_anonymous_stats', false);

// Hosts

host('syamsudin.dev')
    ->stage('production')
    ->roles('app')
    ->set('deploy_path', '~/app/{{ application }}')
    ->user('syamsudin')
    ->port(22884)
    ->set('bin/php', '/usr/local/bin/ea-php74')
    //->addSshOption('ControlMaster', 'no')
;
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

task('app:fix_permission', function () {
    run('cd {{release_path}} && find . -type f -print0 | xargs -0 chmod 644');
    run('cd {{release_path}} && find . -type d -print0 | xargs -0 chmod 755');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'database:migrate');
before('deploy:symlink', 'app:fix_permission');

set('env', [
    'APP_ENV' => 'prod',
    'APP_SECRET' => '',
    'DATABASE_URL' => getenv('DATABASE_URL'),
    'MAILER_URL' => 'smtp://mail.syamsudin.dev:465'
###> symfony/swiftmailer-bundle ###
# For Gmail as a transport, use: "gmail://username:password@localhost"
# For a generic SMTP server, use: "smtp://localhost:25?encryption=&auth_mode="
# Delivery is disabled by default via "null://localhost"
###< symfony/swiftmailer-bundle ###
]);
