<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211014192608 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE design_name (id INT AUTO_INCREMENT NOT NULL, design_id INT DEFAULT NULL, position INT NOT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_3053AA3E41DC9B2 (design_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE design_number (id INT AUTO_INCREMENT NOT NULL, design_id INT DEFAULT NULL, position INT NOT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_34518F57E41DC9B2 (design_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE design_name ADD CONSTRAINT FK_3053AA3E41DC9B2 FOREIGN KEY (design_id) REFERENCES design (id)');
        $this->addSql('ALTER TABLE design_number ADD CONSTRAINT FK_34518F57E41DC9B2 FOREIGN KEY (design_id) REFERENCES design (id)');
        $this->addSql('ALTER TABLE design ADD has_names TINYINT(1) NOT NULL, ADD names_side VARCHAR(10) DEFAULT NULL, ADD names_height INT DEFAULT NULL, ADD names_color VARCHAR(7) DEFAULT NULL, ADD has_numbers TINYINT(1) NOT NULL, ADD numbers_side VARCHAR(10) DEFAULT NULL, ADD numbers_height INT DEFAULT NULL, ADD numbers_color VARCHAR(7) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE design_name');
        $this->addSql('DROP TABLE design_number');
        $this->addSql('ALTER TABLE design DROP has_names, DROP names_side, DROP names_height, DROP names_color, DROP has_numbers, DROP numbers_side, DROP numbers_height, DROP numbers_color');
    }
}
