<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211018021900 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FC35D654D17F50A6 ON art (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_630FC36D17F50A6 ON custom_image (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CD4F5A30D17F50A6 ON design (uuid)');
        $this->addSql('ALTER TABLE `order` ADD paid_order_payment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398AD31B645 FOREIGN KEY (paid_order_payment_id) REFERENCES order_payment (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F5299398D17F50A6 ON `order` (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F529939896901F54 ON `order` (number)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F5299398AD31B645 ON `order` (paid_order_payment_id)');
        $this->addSql('ALTER TABLE order_payment ADD reference_number VARCHAR(500) DEFAULT NULL, ADD payment_details LONGTEXT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649D17F50A6 ON user (uuid)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_FC35D654D17F50A6 ON art');
        $this->addSql('DROP INDEX UNIQ_630FC36D17F50A6 ON custom_image');
        $this->addSql('DROP INDEX UNIQ_CD4F5A30D17F50A6 ON design');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F5299398AD31B645');
        $this->addSql('DROP INDEX UNIQ_F5299398D17F50A6 ON `order`');
        $this->addSql('DROP INDEX UNIQ_F529939896901F54 ON `order`');
        $this->addSql('DROP INDEX UNIQ_F5299398AD31B645 ON `order`');
        $this->addSql('ALTER TABLE `order` DROP paid_order_payment_id');
        $this->addSql('ALTER TABLE order_payment DROP reference_number, DROP payment_details');
        $this->addSql('DROP INDEX UNIQ_8D93D649D17F50A6 ON user');
    }
}
