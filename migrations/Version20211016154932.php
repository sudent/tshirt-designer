<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211016154932 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE art CHANGE is_enabled enabled TINYINT(1) DEFAULT \'1\'');
        $this->addSql('ALTER TABLE custom_image CHANGE is_enabled enabled TINYINT(1) DEFAULT \'1\'');
        $this->addSql('ALTER TABLE product CHANGE is_enabled enabled TINYINT(1) DEFAULT \'1\'');
        $this->addSql('ALTER TABLE user CHANGE is_enabled enabled TINYINT(1) DEFAULT \'1\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE art CHANGE enabled is_enabled TINYINT(1) DEFAULT \'1\'');
        $this->addSql('ALTER TABLE custom_image CHANGE enabled is_enabled TINYINT(1) DEFAULT \'1\'');
        $this->addSql('ALTER TABLE product CHANGE enabled is_enabled TINYINT(1) DEFAULT \'1\'');
        $this->addSql('ALTER TABLE user CHANGE enabled is_enabled TINYINT(1) DEFAULT \'1\'');
    }
}
