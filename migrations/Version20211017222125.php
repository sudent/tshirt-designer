<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211017222125 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE order_item_variant (id INT AUTO_INCREMENT NOT NULL, order_item_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, size VARCHAR(255) NOT NULL, quantity INT NOT NULL, unit_price INT NOT NULL, total INT NOT NULL, INDEX IDX_FEDE0D93E415FB15 (order_item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE order_item_variant ADD CONSTRAINT FK_FEDE0D93E415FB15 FOREIGN KEY (order_item_id) REFERENCES order_item (id)');
        $this->addSql('ALTER TABLE `order` ADD order_shipping_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F529939853358C7E FOREIGN KEY (order_shipping_id) REFERENCES order_shipping (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F529939853358C7E ON `order` (order_shipping_id)');
        $this->addSql('ALTER TABLE order_item DROP size, DROP type');
        $this->addSql('ALTER TABLE order_item_custom_name ADD value VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE order_item_variant');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F529939853358C7E');
        $this->addSql('DROP INDEX UNIQ_F529939853358C7E ON `order`');
        $this->addSql('ALTER TABLE `order` DROP order_shipping_id');
        $this->addSql('ALTER TABLE order_item ADD size VARCHAR(10) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD type VARCHAR(20) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE order_item_custom_name DROP value');
    }
}
