var canvas;
var tshirts = []; //prototype: [{style:'x',color:'white',front:'a',back:'b',price:{tshirt:'12.95',frontPrint:'4.99',backPrint:'4.99',total:'22.47'}}]
var a = null;
var b = null;
var products = {};
var previousProductCode = null;
var line1;
var line2;
var line3;
var line4;
var HideControls = {
    'tl':false,
    'tr':false,
    'bl':false,
    'br':true,
    'ml':false,
    'mt':false,
    'mr':false,
    'mb':false,
    'mtr':false
};
var HideAllControls = {
    'tl':false,
    'tr':false,
    'bl':false,
    'br':false,
    'ml':false,
    'mt':false,
    'mr':false,
    'mb':false,
    'mtr':false
};
var deleteNamePlaceholderOnFlip = false;
var deleteNumberPlaceholderOnFlip = false;
var nameColor = null;
var numberColor = null;
var nameHeight = null;
var numberHeight = null;
var canvas2fabric = null;
var canvas3fabric = null;
var canvasRatio = 1;
var currentWidth = 530;
var currentHeight = 630;
var originalWidth = 530;
var originalHeight = 630;
var frontAlreadyScaledWithOriginal = false;
var backAlreadyScaledWithOriginal = false;
$(document).ready(function() {
    //setup front side canvas
    canvas = new fabric.Canvas('tcanvas', {
        hoverCursor: 'pointer',
        selection: true,
        selectionBorderColor:'blue'
    });
    canvas.on({
        'object:moving': function(e) {
            e.target.opacity = 0.5;
            $(".deleteBtn").remove();
        },
        'object:modified': function(e) {
            e.target.opacity = 1;
            if (!e.target.hasOwnProperty('id'))
                addDeleteBtn(e.target.oCoords.tl.x, e.target.oCoords.tl.y);
        },
        'mouse:up':onObjectSelected,
        'selection:cleared':onSelectedCleared,
        'mouse:down': function(e){
            if(!canvas.getActiveObject())
            {
                $(".deleteBtn").remove();
            } else {
                if (!e.target.hasOwnProperty('id'))
                    addDeleteBtn(e.target.oCoords.tl.x, e.target.oCoords.tl.y);
            }
        },
        'object:scaling': function(){
            $(".deleteBtn").remove();
        }
    });
    // piggyback on `canvas.findTarget`, to fire "object:over" and "object:out" events
    canvas.findTarget = (function(originalFn) {
        return function() {
            var target = originalFn.apply(this, arguments);
            if (target) {
                if (this._hoveredTarget !== target) {
                    canvas.fire('object:over', { target: target });
                    if (this._hoveredTarget) {
                        canvas.fire('object:out', { target: this._hoveredTarget });
                    }
                    this._hoveredTarget = target;
                }
            }
            else if (this._hoveredTarget) {
                canvas.fire('object:out', { target: this._hoveredTarget });
                this._hoveredTarget = null;
            }
            return target;
        };
    })(canvas.findTarget);

    canvas.on('object:over', function(e) {
        //e.target.setFill('red');
        //canvas.renderAll();
    });

    canvas.on('object:out', function(e) {
        //e.target.setFill('green');
        //canvas.renderAll();
    });

    // canvas.on('mouse:out', (opt) => {
    //     if (opt && opt.e && opt.e.target !== canvas.upperCanvasEl) {
    //         console.log('out')
    //         canvas.remove(line1);
    //         canvas.remove(line2);
    //         canvas.remove(line3);
    //         canvas.remove(line4);
    //         canvas.renderAll();
    //     }
    // });
    //
    // canvas.on('mouse:over', (opt) => {
    //     if (opt && opt.e && opt.e.target === canvas.upperCanvasEl) {
    //         console.log('in')
    //
    //         canvas.add(line1);
    //         canvas.add(line2);
    //         canvas.add(line3);
    //         canvas.add(line4);
    //         canvas.renderAll();
    //     }
    // });

    $('#add-text').on('click', function() {
        var text = $("#text-string").val();
        var textSample = new fabric.Text(text, {
            left: 20,
            top: 20,
            fontFamily: 'helvetica',
            angle: 0,
            fill: '#000000',
            fontWeight: '',
            hasRotatingPoint:true,
            textAlign: 'center',
            originX: 'center',
            originY: 'center'
        });
        textSample.setControlsVisibility(HideControls);
        if (isSmallScreen()) {
            textSample.set('scaleX', canvasRatio);
            textSample.set('scaleY', canvasRatio);
            textSample.setCoords();
        }
        canvas.add(textSample);
        canvas.item(canvas.item.length-1).hasRotatingPoint = true;
        canvas.centerObject(textSample);
        textSample.setCoords();
        canvas.setActiveObject(textSample);
        $("#texteditor").css('display', 'inline');
        $("#imageeditor").css('display', 'inline');
        addDeleteBtn(textSample.oCoords.tl.x, textSample.oCoords.tl.y);
        autoClosePaneIfSmallScreen();
    });
    $("#text-string").keyup(function(){
        var activeObject = canvas.getActiveObject();
        if (activeObject && isTextObject(activeObject)) {
            activeObject.set('text', this.value);
            if (activeObject.type === 'curvedText') {
                setTextEffect(activeObject.effect);
            }
            canvas.renderAll();
        }
    });
    $(".img-polaroid").click(function(e){
        var el = e.target;

        fabric.Image.fromURL(el.src, function(image) {
            addImage(image);
        });
    });
    /*document.getElementById('remove-selected').onclick = function() {
        var activeObject = canvas.getActiveObject(),
            activeGroup = canvas.getActiveGroup();
        if (activeObject) {
            canvas.remove(activeObject);
            $("#text-string").val("");
        }
        else if (activeGroup) {
            var objectsInGroup = activeGroup.getObjects();
            canvas.discardActiveGroup();
            objectsInGroup.forEach(function(object) {
                canvas.remove(object);
            });
        }
    };*/
    $('#bring-to-front').on('click',function() {
        var activeObject = canvas.getActiveObject(),
            activeGroup = canvas.getActiveObjects();
        if (activeObject) {
            activeObject.bringToFront();
            canvas.discardActiveObject();
            canvas.requestRenderAll();
        }
        else if (activeGroup.length > 0) {
            activeGroup.forEach(function(object) {
                object.bringToFront();
            });
            canvas.discardActiveObject();
            canvas.requestRenderAll();
        }
    });
    $('#send-to-back').on('click', function() {
        var activeObject = canvas.getActiveObject(),
            activeGroup = canvas.getActiveObjects();
        if (activeObject) {
            activeObject.sendToBack();
            canvas.discardActiveObject();
            canvas.requestRenderAll();
        }
        else if (activeGroup.length > 0) {
            activeGroup.forEach(function(object) {
                object.sendToBack();
            });
            canvas.discardActiveObject();
            canvas.requestRenderAll();
        }
    });
    $("#text-bold").click(function() {
        var activeObject = canvas.getActiveObject();
        if (activeObject && isTextObject(activeObject)) {
            activeObject.set('fontWeight', activeObject.fontWeight === 'bold' ? '' : 'bold');
            canvas.renderAll();
        }
    });
    $("#text-italic").click(function() {
        var activeObject = canvas.getActiveObject();
        if (activeObject && isTextObject(activeObject)) {
            activeObject.set('fontStyle', activeObject.fontStyle === 'italic' ? '' : 'italic');
            canvas.renderAll();
        }
    });
    $("#text-strike").click(function() {
        var activeObject = canvas.getActiveObject();
        if (activeObject && isTextObject(activeObject)) {
            activeObject.set('linethrough', !activeObject.linethrough);
            canvas.renderAll();
        }
    });
    $("#text-underline").click(function() {
        var activeObject = canvas.getActiveObject();
        if (activeObject && isTextObject(activeObject)) {
            activeObject.set('underline', !activeObject.underline);
            canvas.renderAll();
        }
    });
    $("#text-left").click(function() {
        var activeObject = canvas.getActiveObject();
        if (activeObject && isTextObject(activeObject)) {
            activeObject.textAlign = 'left';
            canvas.renderAll();
        }
    });
    $("#text-center").click(function() {
        var activeObject = canvas.getActiveObject();
        if (activeObject && isTextObject(activeObject)) {
            activeObject.textAlign = 'center';
            canvas.renderAll();
        }
    });
    $("#text-right").click(function() {
        var activeObject = canvas.getActiveObject();
        if (activeObject && isTextObject(activeObject)) {
            activeObject.textAlign = 'right';
            canvas.renderAll();
        }
    });
    $("#font-family").change(function() {
        var activeObject = canvas.getActiveObject();
        if (activeObject && isTextObject(activeObject)) {
            activeObject.set({
                fontFamily: this.value
            });
            canvas.renderAll();
        }
    });
    $('#tshirt-bgcolor').minicolors({
        change: function() {
            $('#shirtDiv').css('background-color', this.value);
        }
    });
    $('#text-bgcolor').change(function() {
            var activeObject = canvas.getActiveObject();
            if (activeObject && isTextObject(activeObject)) {
                activeObject.backgroundColor = this.value;
                canvas.renderAll();
            }
        });
    $('#text-fontcolor').change(function() {
            var activeObject = canvas.getActiveObject();
            if (activeObject && isTextObject(activeObject)) {
                activeObject.set('fill', this.value);
                if (activeObject.type === 'curvedText') {
                    setTextEffect(activeObject.effect);
                }
                canvas.renderAll();
            }
        });

    $('#text-strokecolor').change(function() {
            var activeObject = canvas.getActiveObject();
            if (activeObject && isTextObject(activeObject)) {
                activeObject.set('stroke', this.value);
                activeObject.set('strokeWidth', $('#text-strokewidth').val());
                if (activeObject.type === 'curvedText') {
                    setTextEffect(activeObject.effect);
                }
                canvas.renderAll();
            }
        });

    $('#text-strokewidth').change(function() {
        var activeObject = canvas.getActiveObject();
        if (activeObject && isTextObject(activeObject)) {
            activeObject.set('strokeWidth', this.value);
            activeObject.set('stroke', $('#text-strokecolor').val());
            if (activeObject.type === 'curvedText') {
                setTextEffect(activeObject.effect);
            }
            canvas.renderAll();
        }
    });

    fabric.util.addListener(canvas.upperCanvasEl, 'dblclick', function(e) {
        if (canvas.findTarget(e)) {
            let selectedObject = canvas.findTarget(e)
            if (isTextObject(selectedObject)) {
                //display text editor
                if (!selectedObject) return;
                $("#texteditor").css('display', 'inline');
                $("#text-string").val(selectedObject.get('text')).focus();
                $('#text-fontcolor').minicolors('value',selectedObject.get('fill'));
                $('#text-strokecolor').minicolors('value',selectedObject.get('stroke'));
                $("#imageeditor").css('display', 'inline');
                $('.add-text-button').removeClass('fa-plus');
                $('.add-text-button').addClass('fa-edit');
            }
        }
    });

    //canvas.add(new fabric.fabric.Object({hasBorders:true,hasControls:false,hasRotatingPoint:false,selectable:false,type:'rect'}));
    $("#drawingArea").hover(
        function() {
            canvas.add(line1);
            canvas.add(line2);
            canvas.add(line3);
            canvas.add(line4);
            canvas.renderAll();
        },
        function() {
            canvas.remove(line1);
            canvas.remove(line2);
            canvas.remove(line3);
            canvas.remove(line4);
            canvas.renderAll();
        }
    );

    $('.color-preview').click(function(){
        $(".main-editor-area").style.backgroundColor = $(this).css("background-color");
    });

    $('#flip').click(
        function() {
            $("#texteditor").css('display', 'none');
            $("#text-string").val("");
            $("#imageeditor").css('display', 'none');
            $('.deleteBtn').remove();

            if ($(this).attr("data-original-title") === "Show Back View") {
                $(this).attr('data-original-title', 'Show Front View');
                $("#tshirtFacing").attr("src", $(this).data('back-side-image'));
                a = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try
                {
                    var json = JSON.parse(b);
                    canvas.loadFromJSON(b, function () { adjustCanvasObjectsOnLoad('back') });
                }
                catch(e)
                {console.log(e, 'got error')}
            } else {
                $(this).attr('data-original-title', 'Show Back View');
                $("#tshirtFacing").attr("src", $(this).data('front-side-image'));
                b = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try
                {
                    var json = JSON.parse(a);
                    canvas.loadFromJSON(a, function () { adjustCanvasObjectsOnLoad('front') });
                }
                catch(e)
                {console.log(e, 'got error')}
            }

            var item;

            if (deleteNamePlaceholderOnFlip) {
                item = canvas.getItem('name-placeholder');
                if (item && typeof item !== 'undefined') {
                    canvas.remove(item);
                }
                deleteNamePlaceholderOnFlip = false;
            } else {
                item = canvas.getItem('name-placeholder');
                if (item && typeof item !== 'undefined') {
                    item.set('fontSize', nameHeight);
                    item.set('fill', nameColor);
                }
            }

            if (deleteNumberPlaceholderOnFlip) {
                item = canvas.getItem('number-placeholder');
                if (item && typeof item !== 'undefined') {
                    canvas.remove(item);
                }
                deleteNumberPlaceholderOnFlip = false;
            } else {
                item = canvas.getItem('number-placeholder');
                if (item && typeof item !== 'undefined') {
                    item.set('fontSize', numberHeight);
                    item.set('fill', numberColor);
                }
            }

            setTimeout(function() {
                canvas.calcOffset();
            },200);
        });
    $(".clearfix button,a").tooltip();

    $(document).on('click',".deleteBtn",function(){
        if(canvas.getActiveObject())
        {
            canvas.remove(canvas.getActiveObject());
            $(".deleteBtn").remove();

            $('.add-text-button').removeClass('fa-plus fa-edit');
            $('.add-text-button').addClass('fa-plus');

            canvas.remove(line1);
            canvas.remove(line2);
            canvas.remove(line3);
            canvas.remove(line4);
            canvas.renderAll();
        }
    });

    // $(document).click(function(event) {
    //     if (!$(event.target).hasClass('upper-canvas')) {
    //         canvas.discardActiveObject().renderAll();
    //     }
    // });

    // var resizeId = null;
    // $(function() {
    //     $(window).resize(function() {
    //         if(resizeId != null)
    //             clearTimeout(resizeId);
    //         resizeId = setTimeout(handle_resize, 500);
    //     });
    //     console.log( "ready!" );
    //     /* auto size it right away... */
    //     resizeId = setTimeout(handle_resize, 500);
    // });

    $('#navs-left-responsive-link-5').on('click', '.use-uploaded-image', function () {
        fabric.Image.fromURL($(this).data('img-src'), function(image) {
            addImage(image);
        });
    });

    $('.add-names').on('click', function() {
        var item = canvas.getItem('name-placeholder');

        if ($(this).is(':checked')) {
            if (typeof item !== 'undefined' && item) {
                return;
            }

            if ($('.names.side').val() === 'back') {
                if ($('#flip').attr('data-original-title') === 'Show Back View') {
                    $('#flip').attr('data-original-title', 'Show Front View');
                    $("#tshirtFacing").attr("src",  $('#flip').data('back-side-image'));
                    a = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                    canvas.clear();
                    try {
                        var json = JSON.parse(b);
                        canvas.loadFromJSON(b);
                    } catch (e) {
                    }
                }
            } else {
                if ($('#flip').attr('data-original-title') === 'Show Front View') {
                    $('#flip').attr('data-original-title', 'Show Back View');
                    $("#tshirtFacing").attr("src",  $('#flip').data('front-side-image'));
                    b = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                    canvas.clear();
                    try {
                        var json = JSON.parse(a);
                        canvas.loadFromJSON(a);
                    } catch (e) {
                    }
                }
            }

            nameColor = $('.names.minicolors').val();
            var textSample = new fabric.Text('YOUR NAME', {
                fontFamily: 'impact',
                angle: 0,
                fill: nameColor,
                fontWeight: 'bold',
                hasRotatingPoint: true,
                textAlign: 'center',
                originX: 'center',
                originY: 'center',
                id: 'name-placeholder'
            });
            textSample.setControlsVisibility(HideAllControls);
            nameHeight = parseInt($('.names.height').val()) === 1 ? 20 : 40;
            textSample.set('fontSize', nameHeight);
            textSample.set('scaleX', canvasRatio);
            textSample.set('scaleY', canvasRatio);
            textSample.setCoords();
            canvas.add(textSample);
            canvas.centerObjectH(textSample);
            textSample.set('top', 50 * canvasRatio);
            textSample.setCoords();
            canvas.setActiveObject(textSample);
            canvas.renderAll();
            nameHeight = textSample.get('fontSize');
            nameColor = textSample.get('fill');
            deleteNamePlaceholderOnFlip = false;
        } else {
            if (typeof item !== 'undefined' && item) {
                canvas.remove(item);
                $(".deleteBtn").remove();
            } else {
                deleteNamePlaceholderOnFlip = true;
            }
        }
    });

    $('.add-numbers').on('click', function() {
        var item = canvas.getItem('number-placeholder');

        if ($(this).is(':checked')) {
            if (typeof item !== 'undefined' && item) {
                return;
            }

            if ($('.numbers.side').val() === 'back') {
                if ($('#flip').attr('data-original-title') === 'Show Back View') {
                    $('#flip').attr('data-original-title', 'Show Front View');
                    $("#tshirtFacing").attr("src",  $('#flip').data('back-side-image'));
                    a = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                    canvas.clear();
                    try {
                        var json = JSON.parse(b);
                        canvas.loadFromJSON(b);
                    } catch (e) {
                    }
                }
            } else {
                if ($('#flip').attr('data-original-title') === 'Show Front View') {
                    $('#flip').attr('data-original-title', 'Show Back View');
                    $("#tshirtFacing").attr("src", $('#flip').data('front-side-image'));
                    b = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                    canvas.clear();
                    try {
                        var json = JSON.parse(a);
                        canvas.loadFromJSON(a);
                    } catch (e) {
                    }
                }
            }

            numberColor = $('.numbers.minicolors').val();
            var textSample = new fabric.Text('00', {
                fontFamily: 'impact',
                angle: 0,
                fill: numberColor,
                fontWeight: 'bold',
                hasRotatingPoint: true,
                textAlign: 'center',
                originX: 'center',
                originY: 'center',
                id: 'number-placeholder'
            });
            textSample.setControlsVisibility(HideAllControls);
            numberHeight = parseInt($('.numbers.height').val()) * 20;
            textSample.set('fontSize', numberHeight);
            textSample.set('scaleX', canvasRatio);
            textSample.set('scaleY', canvasRatio);
            textSample.setCoords();
            canvas.add(textSample);
            canvas.centerObjectH(textSample);
            textSample.set('top', 170 * canvasRatio);
            textSample.setCoords();
            canvas.setActiveObject(textSample);
            canvas.renderAll();
            deleteNumberPlaceholderOnFlip = false;
        } else {
            if (typeof item !== 'undefined' && item) {
                canvas.remove(item);
                $(".deleteBtn").remove();
            } else {
                deleteNumberPlaceholderOnFlip = true;
            }
        }
    });

    $('.names.height').change(function() {
        if ($('.names.side').val() === 'back') {
            if ($('#flip').attr('data-original-title') === 'Show Back View') {
                $('#flip').attr('data-original-title', 'Show Front View');
                $("#tshirtFacing").attr("src", $('#flip').data('back-side-image'));
                a = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(b);
                    canvas.loadFromJSON(b);
                } catch (e) {
                }
            }
        } else {
            if ($('#flip').attr('data-original-title') === 'Show Front View') {
                $('#flip').attr('data-original-title', 'Show Back View');
                $("#tshirtFacing").attr("src", $('#flip').data('front-side-image'));
                b = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(a);
                    canvas.loadFromJSON(a);
                } catch (e) {
                }
            }
        }

        var object = canvas.getItem('name-placeholder');

        object.set('fontSize', parseInt($(this).val()) === 1 ? 20 : 40);
        nameHeight = parseInt($(this).val()) === 1 ? 20 : 40;
        canvas.renderAll();
    });

    $('.numbers.height').change(function() {
        if ($('.numbers.side').val() === 'back') {
            if ($('#flip').attr('data-original-title') === 'Show Back View') {
                $('#flip').attr('data-original-title', 'Show Front View');
                $("#tshirtFacing").attr("src", $('#flip').data('back-side-image'));
                a = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(b);
                    canvas.loadFromJSON(b);
                } catch (e) {
                }
            }
        } else {
            if ($('#flip').attr('data-original-title') === 'Show Front View') {
                $('#flip').attr('data-original-title', 'Show Back View');
                $("#tshirtFacing").attr("src", $('#flip').data('front-side-image'));
                b = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(a);
                    canvas.loadFromJSON(a);
                } catch (e) {
                }
            }
        }

        var object = canvas.getItem('number-placeholder');

        object.set('fontSize', parseInt($(this).val()) * 20);
        numberHeight = parseInt($(this).val()) * 20;
        canvas.renderAll();
    });

    $('.names.minicolors').change(function() {
        if (!$('.add-names').is(':checked')) {
            return;
        }

        if ($('.names.side').val() === 'back') {
            if ($('#flip').attr('data-original-title') === 'Show Back View') {
                $('#flip').attr('data-original-title', 'Show Front View');
                $("#tshirtFacing").attr("src", $('#flip').data('back-side-image'));
                a = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(b);
                    canvas.loadFromJSON(b);
                } catch (e) {
                }
            }
        } else {
            if ($('#flip').attr('data-original-title') === 'Show Front View') {
                $('#flip').attr('data-original-title', 'Show Back View');
                $("#tshirtFacing").attr("src", $('#flip').data('front-side-image'));
                b = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(a);
                    canvas.loadFromJSON(a);
                } catch (e) {
                }
            }
        }

        var object = canvas.getItem('name-placeholder');

        object.set('fill', this.value);
        nameColor = this.value;
        canvas.renderAll();
    });

    $('.numbers.minicolors').change(function() {
        if (!$('.add-numbers').is(':checked')) {
            return;
        }

        if ($('.numbers.side').val() === 'back') {
            if ($('#flip').attr('data-original-title') === 'Show Back View') {
                $('#flip').attr('data-original-title', 'Show Front View');
                $("#tshirtFacing").attr("src", $('#flip').data('back-side-image'));
                a = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(b);
                    canvas.loadFromJSON(b);
                } catch (e) {
                }
            }
        } else {
            if ($('#flip').attr('data-original-title') === 'Show Front View') {
                $('#flip').attr('data-original-title', 'Show Back View');
                $("#tshirtFacing").attr("src", $('#flip').data('front-side-image'));
                b = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(a);
                    canvas.loadFromJSON(a);
                } catch (e) {
                }
            }
        }

        var object = canvas.getItem('number-placeholder');

        object.set('fill', this.value);
        numberColor = this.value;
        canvas.renderAll();
    });

    $('.names.side').change(function() {
        var changeSide = false;
        if ($(this).val() === 'back') {
            if ($('#flip').attr('data-original-title') === 'Show Back View') {
                var item = canvas.getItem('name-placeholder');
                canvas.remove(item);
                $('#flip').attr('data-original-title', 'Show Front View');
                $("#tshirtFacing").attr("src", $('#flip').data('back-side-image'));
                a = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(b);
                    canvas.loadFromJSON(b);
                } catch (e) {
                }
            } else {
                changeSide = true;
            }
        } else {
            if ($('#flip').attr('data-original-title') === 'Show Front View') {
                var item = canvas.getItem('name-placeholder');
                canvas.remove(item);
                $('#flip').attr('data-original-title', 'Show Back View');
                $("#tshirtFacing").attr("src", $('#flip').data('front-side-image'));
                b = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(a);
                    canvas.loadFromJSON(a);
                } catch (e) {
                }
            } else {
                changeSide = true;
            }
        }

        var textSample = new fabric.Text('YOUR NAME', {
            fontFamily: 'impact',
            angle: 0,
            fill: $('.names.minicolors').val(),
            fontWeight: 'bold',
            hasRotatingPoint: true,
            textAlign: 'center',
            originX: 'center',
            originY: 'center',
            id: 'name-placeholder'
        });
        textSample.setControlsVisibility(HideAllControls);
        textSample.set('fontSize', parseInt($('.names.height').val()) === 1 ? 20 : 40);
        if (isSmallScreen()) {
            textSample.set('scaleX', canvasRatio);
            textSample.set('scaleY', canvasRatio);
            textSample.setCoords();
        }
        canvas.add(textSample);
        canvas.item(canvas.item.length - 1).hasRotatingPoint = true;
        canvas.centerObjectH(textSample);
        textSample.set('top', 50);
        textSample.setCoords();
        canvas.renderAll();
        if (changeSide) {
            deleteNamePlaceholderOnFlip = true;
        }
        $(this).data('original-side', $(this).val());
    });

    $('.numbers.side').change(function() {
        var changeSide = false;
        if ($(this).val() === 'back') {
            if ($('#flip').attr('data-original-title') === 'Show Back View') {
                var item = canvas.getItem('number-placeholder');
                canvas.remove(item);
                $('#flip').attr('data-original-title', 'Show Front View');
                $("#tshirtFacing").attr("src", $('#flip').data('back-side-image'));
                a = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(b);
                    canvas.loadFromJSON(b);
                } catch (e) {
                }
            } else {
                changeSide = true;
            }
        } else {
            if ($('#flip').attr('data-original-title') === 'Show Front View') {
                $('#flip').attr('data-original-title', 'Show Back View');
                var item = canvas.getItem('number-placeholder');
                canvas.remove(item);
                $("#tshirtFacing").attr("src", $('#flip').data('front-side-image'));
                b = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                canvas.clear();
                try {
                    var json = JSON.parse(a);
                    canvas.loadFromJSON(a);
                } catch (e) {
                }
            } else {
                changeSide = true;
            }
        }

        var textSample = new fabric.Text('00', {
            fontFamily: 'impact',
            angle: 0,
            fill: $('.numbers.minicolors').val(),
            fontWeight: 'bold',
            hasRotatingPoint: true,
            textAlign: 'center',
            originX: 'center',
            originY: 'center',
            id: 'number-placeholder'
        });
        textSample.setControlsVisibility(HideAllControls);
        textSample.set('fontSize', parseInt($('.numbers.height').val()) * 20);
        if (isSmallScreen()) {
            textSample.set('scaleX', canvasRatio);
            textSample.set('scaleY', canvasRatio);
            textSample.setCoords();
        }
        canvas.add(textSample);
        canvas.item(canvas.item.length - 1).hasRotatingPoint = true;
        canvas.centerObjectH(textSample);
        textSample.set('top', 170);
        textSample.setCoords();
        canvas.renderAll();
        if (changeSide) {
            deleteNumberPlaceholderOnFlip = true;
        }
        $(this).data('original-side', $(this).val());
    });
    $('INPUT.minicolors').minicolors();
    $('.upload-image-btn').click(function() {
        document.getElementsByClassName("dropzone")[0].click();
    });
    $('.add-names').click(function() {
        if ($(this).is(':checked')) {
            $('.names').prop('disabled', false);
            deleteNamePlaceholderOnFlip = false;
            $('.add-name-number-btn').prop('disabled', false);
            $('.names-and-numbers-area').removeClass('d-none');
        } else {
            $('.names').prop('disabled', true);
            deleteNamePlaceholderOnFlip = true;

            if (!$('.add-numbers').is(':checked')) {
                $('.add-name-number-btn').prop('disabled', true);
                $('.names-and-numbers-area').addClass('d-none');
            } else {
                $('.add-name-number-btn').prop('disabled', false);
                $('.names-and-numbers-area').removeClass('d-none');
            }
        }
    });
    $('.add-numbers').click(function() {
        if ($(this).is(':checked')) {
            $('.numbers').prop('disabled', false);
            deleteNumberPlaceholderOnFlip = false;
            $('.add-name-number-btn').prop('disabled', false);
            $('.names-and-numbers-area').removeClass('d-none');
        } else {
            $('.numbers').prop('disabled', true);
            deleteNumberPlaceholderOnFlip = true;

            if (!$('.add-names').is(':checked')) {
                $('.add-name-number-btn').prop('disabled', true);
                $('.names-and-numbers-area').addClass('d-none');
            } else {
                $('.add-name-number-btn').prop('disabled', false);
                $('.names-and-numbers-area').removeClass('d-none');
            }
        }
    });
    $('#file-upload').dropzone({
        url: $('#file-upload').data('url'),
        parallelUploads: 2,
        maxFilesize:     10,
        acceptedFiles: 'image/png,image/jpg,image/jpeg',
        thumbnailWidth:"150",
        previewTemplate: '<div class="dz-preview dz-file-preview">\n' +
            '    <div class="dz-details">\n' +
            '        <div class="dz-thumbnail"><img data-dz-thumbnail> <span class="dz-nopreview">No preview</span>\n' +
            '            <div class="dz-success-mark"></div>\n' +
            '            <div class="dz-error-mark"></div>\n' +
            '            <div class="dz-error-message"><span data-dz-errormessage></span></div>\n' +
            '            <div class="progress">\n' +
            '                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100"\n' +
            '                     data-dz-uploadprogress></div>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>',
        init: function() {
            if (isLoggedIn) {
                let myDropzone = this;
                $.ajax({
                    type: 'get',
                    url: Routing.generate('endpoint_user_images'),
                    success: function(mocks){
                        $.each(mocks, function(key,value) {
                            let mockFile = { name: value.file_name, size: 1024 };
                            myDropzone.displayExistingFile(mockFile, value.url);
                            var $previewElement = $(mockFile.previewElement);
                            var $addImageButton = $('<div class="text-center"><button class="btn btn-warning btn-sm m-2 use-uploaded-image" type="button">Use Image</button></div>')
                            $previewElement.find('.dz-details').append($addImageButton);
                            $previewElement.find('.use-uploaded-image').data('img-src', value.url);
                        });
                    }
                });
            }
            this.on("success", function(file, serverResponse) {
                // Called after the file successfully uploaded.
                var $previewElement = $(file.previewElement);
                var $addImageButton = $('<div class="text-center"><button class="btn btn-warning btn-sm m-2 use-uploaded-image" type="button">Use Image</button></div>')
                $previewElement.find('.dz-details').append($addImageButton);
                $previewElement.find('.use-uploaded-image').data('img-src', serverResponse.src);
            });
        }

    });

    var $collectionHolder = $('.name-and-number-tbody');

    if ($('.add-names').is(':checked') || $('.add-numbers').is(':checked')) {
        $('.add-name-number-btn').prop('disabled', false);
        $('.names-and-numbers-area').removeClass('d-none');
    }

    $('.add-name-number-btn').click(function() {
        // var length = $('.name-number-row').length;
        // var template = '<tr class="name-number-row">\n' +
        //     '<td>' + parseInt(length+1) + '.</td>\n' +
        //     '<td><input class="form-control names"' + ($('.add-names').is(':checked') ? '' : ' disabled') + '></td>\n' +
        //     '<td><input class="form-control numbers"' + ($('.add-numbers').is(':checked') ? '' : ' disabled') + '></td>\n' +
        //     '</tr>';
        //$('.name-and-number-tbody').append(template);

        var namePrototype = $collectionHolder.data('name-prototype');
        var numberPrototype = $collectionHolder.data('number-prototype');
        var index = $collectionHolder.data('index');

        namePrototype = namePrototype.replace(/__name__/g, index);
        numberPrototype = numberPrototype.replace(/__name__/g, index);

        $collectionHolder.data('index', index + 1);

        var $newElem = $('<tr class="name-number-row"><td class="index-column"></td><td class="name-column"></td><td class="number-column"></td></tr>');

        $newElem.find('.index-column').text(index + 1);
        $newElem.find('.name-column').html(namePrototype);

        if (!$('.add-names').is(':checked')) {
            $newElem.find('.name-column input').prop('disabled', true);
        }

        $newElem.find('.number-column').html(numberPrototype);
        if (!$('.numbers').is(':checked')) {
            $newElem.find('.number-column input').prop('disabled', true);
        }

        $collectionHolder.append($newElem);
    });

    $('.remove-name-number-btn').click(function() {
        var length = $('.name-number-row').length;

        if (length === 0) {
            return;
        }

        if (length > 1) {
            $('.name-and-number-tbody').children().last().remove();
        } else {
            alert('A minimum of one name/number is required.');
            return;
        }

        $('.name-and-number-tbody').data('index', length - 1);
    });

    $('.preview-btn').click(function () {
        if ($(this).hasClass('active')) {
            return;
        }

        $('.images-result').empty();
        finalize().done(function(frontSideJsonData, backSideJsonData, frontSideThumbnail, backSideThumbnail) {
            $('#design_frontSideJsonData').val(frontSideJsonData);
            $('#design_backSideJsonData').val(backSideJsonData);
            $('#design_frontSideThumbnail').val(frontSideThumbnail);
            $('#design_backSideThumbnail').val(backSideThumbnail);
        });
    });

    $('#save-btn').click(function () {
        $('#design_action').val('save');
        $('#design_originalWidth').val(currentWidth);
        $('#design_originalHeight').val(currentHeight);
        $('#design-form').submit();
    });

    $('#add-to-cart-btn').click(function () {
        $('#design_action').val('add-to-cart');
        $('#design_originalWidth').val(currentWidth);
        $('#design_originalHeight').val(currentHeight);
        $('#design-form').submit();
    });

    $('.quantities input.form-control[type="number"]').change(function() {
        var totalQuantities = 0;
        $('.quantities input.form-control[type="number"]').each(function() {
            if ($(this).val() !== '' && parseInt($(this).val()) > 0) {
                totalQuantities += parseInt($(this).val());
                $(this).addClass('font-weight-bold');
                $(this).addClass('bg-success');
            } else {
                $(this).removeClass('bg-success')
                $(this).removeClass('font-weight-bold');
            }
        });
        $('.total-quantities').text(totalQuantities);
    });

    $('.select-product').click(function() {
        $('.select-product').removeClass('product-selected');
        $('.product').removeClass('bg-primary');
        $('.product').addClass('bg-secondary');

        var productCode = $(this).data('product-code');
        $('#design_product').val(productCode);

        if (previousProductCode !== null && previousProductCode !== productCode) {
            if ($('#flip').attr('data-original-title') === 'Show Back View') {
                a = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
            } else {
                b = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]))
            }
            products[previousProductCode] = {
                front: a,
                back: b,
                bgColor: $('#tshirt-bgcolor').val()
            };
        } else if (previousProductCode === productCode) {
            return;
        }

        $(this).addClass('product-selected');
        $('.product-' + productCode).addClass('bg-primary');
        $('.editor-tab').removeClass('d-none');
        $('.editor-panel').removeClass('d-none');

        $('.editor-area').removeClass('d-none');

        $('#flip').data('front-side-image', $(this).data('front-side-image'));
        $('#flip').data('back-side-image', $(this).data('back-side-image'));
        $('#fcanvas').data('bg', $(this).data('front-side-image'));
        $('#bcanvas').data('bg', $(this).data('back-side-image'));
        $('#tshirtFacing').attr('src', $(this).data('front-side-image'));

        previousProductCode = productCode;

        if (products.hasOwnProperty(productCode)) {
            $('#flip').prop('disabled', true);
            a = products[productCode].front;
            b = products[productCode].back;

            $('#shirtDiv').css('background-color', products[productCode].bgColor);
            $('#tshirt-bgcolor').minicolors('value', products[productCode].bgColor);

            canvas.clear();

            try
            {
                if ($('#flip').attr('data-original-title') === 'Show Back View') {
                    var json = JSON.parse(a);
                    canvas.loadFromJSON(a);
                } else {
                    var json = JSON.parse(b);
                    canvas.loadFromJSON(b);
                }

                setTimeout(function() {
                    canvas.calcOffset();
                    $('#flip').prop('disabled', false);
                    autoClosePaneIfSmallScreen();
                },200);
            }
            catch(e)
            {}
        } else {
            autoClosePaneIfSmallScreen();
        }
    });

    if ($('#design_originalWidth').val() !== '') {
        originalWidth = parseFloat($('#design_originalWidth').val());
    } else {
        originalWidth = isSmallScreen() ? calculateWindowWidth() : 530;
    }

    if ($('#design_originalHeight').val() !== '') {
        originalHeight = parseFloat($('#design_originalHeight').val());
    } else {
        originalHeight = isSmallScreen() ? 630 * (calculateWindowWidth() / 530) : 630;
    }

    repaintPanelTabs();
    recalculateCanvasDimension();

    if ($('#tshirt-bgcolor').val() !== '') {
        $('#shirtDiv').css('background-color', $('#tshirt-bgcolor').val());
    }

    if ($('#design_product').val() !== '') {
        var $selectedProduct = $('.product.product-' + $('#design_product').val());
        var $selectedProductLink = $selectedProduct.find('.select-product');
        $selectedProduct.removeClass('bg-secondary');
        $selectedProduct.addClass('bg-primary');
        $selectedProductLink.addClass('product-selected');

        $('.editor-tab').removeClass('d-none');
        $('.editor-panel').removeClass('d-none');

        $('.editor-area').removeClass('d-none');

        $('#flip').data('front-side-image', $selectedProductLink.data('front-side-image'));
        $('#flip').data('back-side-image', $selectedProductLink.data('back-side-image'));
        $('#fcanvas').data('bg', $selectedProductLink.data('front-side-image'));
        $('#bcanvas').data('bg', $selectedProductLink.data('back-side-image'));
        $('#tshirtFacing').attr('src', $selectedProductLink.data('front-side-image'));
    }

    if ($('#design_frontSideJsonData').val() !== '') {
        if ($('#flip').attr('data-original-title') === 'Show Back View') {
            canvas.clear();
            try
            {
                var json = JSON.parse($('#design_frontSideJsonData').val());
                canvas.loadFromJSON($('#design_frontSideJsonData').val(), function() {
                    adjustCanvasObjectsOnLoad('front')
                });
            }
            catch(e)
            {}
        } else {
            try
            {
                a = $('#design_frontSideJsonData').val();
            }
            catch(e)
            {}
        }
    }

    if ($('#design_backSideJsonData').val() !== '') {
        if ($('#flip').attr('data-original-title') === 'Show Front View') {
            canvas.clear();
            try
            {
                var json = JSON.parse($('#design_backSideJsonData').val());
                canvas.loadFromJSON($('#design_backSideJsonData').val(), function() {
                    adjustCanvasObjectsOnLoad('back')
                });
            }
            catch(e)
            {}
        } else {
            try
            {
                b = $('#design_backSideJsonData').val();
            }
            catch(e)
            {}
        }
    }

    if (!$('#design_hasNames').is(':checked')) {
        $('#design_namesSide').val('back');
        $('#design_namesHeight').val(2);
        $('.names.minicolors').minicolors('value', '#000000');
        $('.names').prop('disabled', true);
    } else {
        nameHeight = parseInt($('#design_namesHeight').val()) === 1 ? 20 : 40;
        nameColor = $('.names.minicolors').val();
    }

    if (!$('#design_hasNumbers').is(':checked')) {
        $('#design_numbersSide').val('back');
        $('#design_numbersHeight').val(8);
        $('.numbers.minicolors').minicolors('value', '#000000');
        $('.numbers').prop('disabled', true);
    } else {
        numberHeight = parseInt($('#design_numbersHeight').val()) * 20;
        numberColor = $('.numbers.minicolors').val();
    }

    // Make row same
    if ($('#design_hasNames').is(':checked') || $('#design_hasNumbers').is(':checked')) {

        $('.name-number-row').each(function(index) {
            if ($(this).find('.name-column input').length === 0) {
                var namePrototype = $collectionHolder.data('name-prototype');

                namePrototype = namePrototype.replace(/__name__/g, index);
                $(this).find('.name-column').append(namePrototype);

                if (!$('#design_hasNames').is(':checked')) {
                    $(this).find('.name-column').find('input').prop('disabled', true);
                }
            }

            if ($(this).find('.number-column input').length === 0) {
                var numberPrototype = $collectionHolder.data('number-prototype');

                numberPrototype = numberPrototype.replace(/__name__/g, index);
                $(this).find('.number-column').append(numberPrototype);

                if (!$('#design_hasNumbers').is(':checked')) {
                    $(this).find('.number-column').find('input').prop('disabled', true);
                }
            }
        });
        //
        // var totalRows = $('.name-number-row').length;
        //
        // if (totalRows !== $('.name-column .position-input.names').length) {
        //     var currentLength = $('.name-column .position-input.names').length;
        //     while(currentLength < totalRows) {
        //         var namePrototype = $collectionHolder.data('name-prototype');
        //
        //         namePrototype = namePrototype.replace(/__name__/g, currentLength);
        //
        //         currentLength++;
        //
        //         var $newElem = $('<tr class="name-number-row"><td class="index-column"></td><td class="name-column"></td><td class="number-column"></td></tr>');
        //
        //         $newElem.find('.index-column').text(index + 1);
        //         $newElem.find('.name-column').html(namePrototype);
        //
        //         if (!$('.add-names').is(':checked')) {
        //             $newElem.find('.name-column input').prop('disabled', true);
        //         }
        //     }
        // }
    }

    var overlayBg = '#22252B';

    $('.confirm-btn').click(function(e) {
        e.preventDefault();
        $('#design-form').submit();
        return false;
        $(this).prop('disable', true);

        $.blockUI({
            message: '<div class="sk-chase sk-warning mx-auto mb-4"><div class="sk-chase-dot"></div><div class="sk-chase-dot"></div><div class="sk-chase-dot"></div><div class="sk-chase-dot"></div><div class="sk-chase-dot"></div><div class="sk-chase-dot"></div></div><h5 class="text-body simulation-text">Simulating payment...</h5>',
            css: {
                backgroundColor: 'transparent',
                border: '0',
                zIndex: 9999999
            },
            overlayCSS:  {
                backgroundColor: overlayBg,
                opacity: 0.8,
                zIndex: 9999990
            }
        });


        setTimeout(function() {
            $('.simulation-text').text('Payment completed! Redirecting to success page.');
            setTimeout(function() {
                $.unblockUI();
                $(this).prop('disable', false);
            }, 5000);
        }, 10000);
    });

    $('#copy-design-link-btn').tooltip();

    if (Clipboard.isSupported()) {
        new Clipboard('.clipboard-btn');
    } else {
        $('.clipboard-btn').prop('disabled', true);
    }

    $('.nav-link').on('click', function (e) {
        handleTabLinkClicked(this, e)
    });

    line1 = new fabric.Line([0,0,200*canvasRatio,0], {"stroke":"#000000", "strokeWidth":1,strokeUniform:true,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false,strokeDashArray:[5,5]});
    line2 = new fabric.Line([199*canvasRatio,0,199*canvasRatio,400*canvasRatio], {stroke: '#000000', "strokeWidth":1,strokeUniform:true,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false,strokeDashArray:[5,5]});
    line3 = new fabric.Line([0,0,0,400*canvasRatio], {"stroke":"#000000", "strokeWidth":1,strokeUniform:true,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false,strokeDashArray:[5,5]});
    line4 = new fabric.Line([0,399*canvasRatio,200*canvasRatio,399*canvasRatio], {stroke: '#000000', "strokeWidth":1,strokeUniform:true,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false,strokeDashArray:[5,5]});

    window.layoutHelpers.on('resize', function() {
        repaintPanelTabs();
    });
});//doc ready

(function() {
    window.layoutHelpers.setAutoUpdate(true);
})();

function repaintPanelTabs()
{
    if (window.layoutHelpers.isSmallScreen()) {
        $('#names-and-numbers-text').html('Names & Numbers');

        $('#panel-tabs').removeClass('nav-tabs-left');
        $('#panel-tabs').addClass('nav-tabs-bottom fixed-bottom');

        setTabPanesHeight();

        if ($('#panel-tabs .nav-link.active').length === 0) {
            hideTabPanes();
        }
    } else {
        $('#panel-tabs').removeClass('nav-tabs-bottom fixed-bottom');
        $('#panel-tabs').addClass('nav-tabs-left');
        $('#names-and-numbers-text').html('Names &<br>Numbers');
        $('#product-type-nav-link').tab('show');
        showTabPanes();
    }
}

function hideTabPanes()
{
    $('.tab-content').addClass('d-none');
}

function showTabPanes()
{
    $('.tab-content').removeClass('d-none');
}

function setTabPanesHeight()
{
    var paneHeight = $(window).height() - $('.layout-navbar').outerHeight() - $('.nav.nav-tabs').outerHeight();
    $('.tab-content').height(paneHeight);
}

function handleTabLinkClicked(elem, e = null)
{
    if (window.layoutHelpers.isSmallScreen()) {
        if ($(elem).hasClass('active')) {
            $($(elem).attr('href')).removeClass('active show');
            $(elem).removeClass("active");
            hideTabPanes();
            if (e !== null) {
                e.preventDefault();
                e.stopPropagation();
            }
        } else {
            showTabPanes();
        }
    }
}

function adjustCanvasObjectsOnLoad(side, fabricCanvas = null) {
    if (side === 'front' && frontAlreadyScaledWithOriginal || side === 'back' && backAlreadyScaledWithOriginal) {
        return;
    }

    var widthRatio = 1;
    var heightRatio = 1;

    if (currentWidth > originalWidth) {
        widthRatio = originalWidth / currentWidth;
        heightRatio = originalHeight / currentHeight;
    } else if (currentWidth < originalWidth) {
        widthRatio = 1 - (currentWidth / originalWidth);
        heightRatio = 1 - (currentHeight / originalHeight);
    } else {
        if (side === 'front') {
            frontAlreadyScaledWithOriginal = true;
        } else {
            backAlreadyScaledWithOriginal = true;
        }

        return;
    }

    let _canvas;

    if (fabricCanvas !== null) {
        _canvas = fabricCanvas;
    } else {
        _canvas = canvas;
    }

    _canvas.getObjects().map(function(o) {
        if (currentWidth > originalWidth) {
            o.set('left', o.left + (o.left * widthRatio));
            o.set('top', o.top + (o.top * heightRatio));
            o.set('scaleX', o.get('scaleX') + (o.get('scaleX') * widthRatio));
            o.set('scaleY', o.get('scaleY') + (o.get('scaleY') * heightRatio));
        } else if (currentWidth < originalWidth) {
            o.set('left', o.left - (o.left * widthRatio));
            o.set('top', o.top - (o.top * heightRatio));
            o.set('scaleX', o.get('scaleX') - (o.get('scaleX') * widthRatio));
            o.set('scaleY', o.get('scaleY') - (o.get('scaleY') * heightRatio));
        }

        o.setCoords();
    });

    _canvas.renderAll();

    if (side === 'front') {
        a = JSON.stringify(_canvas.toJSON(["_controlsVisibility", "id"]));
        frontAlreadyScaledWithOriginal = true;
    } else {
        b = JSON.stringify(_canvas.toJSON(["_controlsVisibility", "id"]));
        backAlreadyScaledWithOriginal = true;
    }
}

function adjustCanvasObjectsToSize(side, _canvas, toWidth = 0, toHeight = 0) {
    if (toWidth === 0 && toHeight === 0) {
        return;
    }

    let _width;
    let _height;

    if (side === 'front') {
        _width = frontAlreadyScaledWithOriginal ? currentWidth : originalWidth;
        _height = frontAlreadyScaledWithOriginal ? currentHeight : originalHeight;
    } else {
        _width = backAlreadyScaledWithOriginal ? currentWidth : originalWidth;
        _height = backAlreadyScaledWithOriginal ? currentHeight : originalHeight;
    }

    let widthRatio = _width < toWidth ? _width / toWidth : toWidth / _width;
    let heightRatio = _height < toHeight ? _height / toHeight : toHeight / _height;

    _canvas.getObjects().map(function(o) {
        if (_width < toWidth) {
            o.set('left', o.left + (o.left * widthRatio));
            o.set('top', o.top + (o.top * heightRatio));
            o.set('scaleX', o.get('scaleX') + (o.get('scaleX') * widthRatio));
            o.set('scaleY', o.get('scaleY') + (o.get('scaleY') * heightRatio));
        } else if (_width > toWidth) {
            o.set('left', o.left - (o.left * widthRatio));
            o.set('top', o.top - (o.top * heightRatio));
            o.set('scaleX', o.get('scaleX') - (o.get('scaleX') * widthRatio));
            o.set('scaleY', o.get('scaleY') - (o.get('scaleY') * heightRatio));
        }

        o.setCoords();
    });

    _canvas.renderAll();
}

function isSmallScreen()
{
    return window.layoutHelpers.isSmallScreen();
}

function autoClosePaneIfSmallScreen()
{
    if (isSmallScreen() && $('#panel-tabs .nav-link.active').length > 0) {
        $($('#panel-tabs .nav-link.active').attr('href')).removeClass('active show');
        $('#panel-tabs .nav-link.active').removeClass("active");
        hideTabPanes();
    }
}

function recalculateCanvasDimension()
{
    if (!isSmallScreen()) {
        return;
    }

    currentWidth = calculateWindowWidth();
    var ratio = currentWidth / 530;
    currentHeight = ratio * 630;
    canvasRatio = ratio;
    var $shirtDiv = $('#shirtDiv');
    var $drawingArea = $('#drawingArea');
    var $canvas = $('#tcanvas');
    var drawingAreaTop = 100 * ratio;
    var drawingAreaLeft = 160 * ratio;
    var drawingAreaWidth = 200 * ratio;
    var drawingAreaHeight = 400 * ratio;
    $shirtDiv.width(currentWidth);
    $shirtDiv.height(currentHeight);
    $drawingArea.width(drawingAreaWidth);
    $drawingArea.height(drawingAreaHeight);
    $drawingArea.css({top: drawingAreaTop + 'px', left: drawingAreaLeft + 'px'});
    $canvas.width(drawingAreaWidth);
    $canvas.height(drawingAreaHeight);
    canvas.setHeight(drawingAreaHeight);
    canvas.setWidth(drawingAreaWidth);
    canvas.renderAll();

    $('#texteditor .btn-group').addClass('btn-group-sm');
    $('#imageeditor .btn-group').addClass('btn-group-sm');
}

function calculateWindowWidth()
{
    return $(window).width() - ($('.container-fluid').outerWidth() - $('.container-fluid').width());
}

function finalize()
{
    var deferred = $.Deferred();

    $.blockUI({
        message: '<div class="sk-chase sk-warning mx-auto mb-4"><div class="sk-chase-dot"></div><div class="sk-chase-dot"></div><div class="sk-chase-dot"></div><div class="sk-chase-dot"></div><div class="sk-chase-dot"></div><div class="sk-chase-dot"></div></div><h5 class="text-body simulation-text">Please wait...</h5>',
        css: {
            backgroundColor: 'transparent',
            border: '0',
            zIndex: 9999999
        },
        overlayCSS:  {
            backgroundColor: '#22252B',
            opacity: 0.8,
            zIndex: 9999990
        }
    });

    canvas2fabric = new fabric.Canvas('fcanvas');

    if ($('#flip').attr("data-original-title") === "Show Back View") {
        a = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
    }

    if (a !== null) {
        var json = JSON.parse(a);
        canvas2fabric.loadFromJSON(a, function() { adjustCanvasObjectsToSize('front', canvas2fabric, 530, 630); });
    }

    try {
        setTimeout(function() {
            var objs = canvas2fabric.getObjects().map(function(o) {
                o.set('left', o.left + 160);
                o.set('top', o.top + 100);

                o.setCoords();
            });
            var textSample = new fabric.Text('FRONT SIDE', {
                fontFamily: 'arial',
                angle: 0,
                fill: '#775cdc',
                fontWeight: '',
                textAlign: 'center',
                originX: 'center',
                originY: 'center'
            });
            textSample.setControlsVisibility(HideControls);
            canvas2fabric.add(textSample);
            canvas2fabric.centerObject(textSample);
            textSample.set('top', canvas2fabric.height - (30 * canvasRatio));
            textSample.setCoords();

            canvas3fabric = new fabric.Canvas('bcanvas');
            try {
                if ($('#flip').attr("data-original-title") === "Show Front View") {
                    b = JSON.stringify(canvas.toJSON(["_controlsVisibility", "id"]));
                }

                if (b !== null) {
                    var json = JSON.parse(b);
                    canvas3fabric.loadFromJSON(b, function() { adjustCanvasObjectsToSize('back', canvas3fabric, 530, 630); });
                }

                setTimeout(function() {
                    var objs = canvas3fabric.getObjects().map(function(o) {
                        o.set('left', o.left + 160);
                        o.set('top', o.top + 100);

                        o.setCoords();
                    });

                    var textSample2 = new fabric.Text('BACK SIDE', {
                        fontFamily: 'Arial',
                        angle: 0,
                        fill: '#775cdc',
                        textAlign: 'center',
                        originX: 'center',
                        originY: 'center'
                    });
                    textSample2.setControlsVisibility(HideControls);
                    canvas3fabric.add(textSample2);
                    canvas3fabric.centerObject(textSample2);
                    textSample2.set('top', canvas3fabric.height - (30 * canvasRatio));
                    textSample2.setCoords();
                    canvas3fabric.renderAll();

                    setTimeout(function() {
                        canvas2fabric.calcOffset();
                        canvas2fabric.renderAll();

                        canvas2fabric.setBackgroundImage($('#fcanvas').data('bg'), canvas2fabric.renderAll.bind(canvas2fabric), {
                            // should the image be resized to fit the container?
                            backgroundImageStretch: false
                        });
                        canvas2fabric.setBackgroundColor($('#tshirt-bgcolor').val());
                        setTimeout(function() {
                            canvas2fabric.calcOffset();
                            var front = canvas2fabric.toDataURL('png');
                            var $row = $('<div class="col-12 col-lg-6 mb-4"><img class="img-fluid img-front border border-primary" style="border-width: 5px !important;"></div>');
                            $row.find('.img-front').attr('src', front);
                            $('.images-result').append($row);

                            setTimeout(function() {
                                canvas3fabric.calcOffset();
                                canvas3fabric.renderAll();

                                canvas3fabric.setBackgroundImage($('#bcanvas').data('bg'), canvas3fabric.renderAll.bind(canvas3fabric), {
                                    // should the image be resized to fit the container?
                                    backgroundImageStretch: false
                                });
                                canvas3fabric.setBackgroundColor($('#tshirt-bgcolor').val());
                                setTimeout(function() {
                                    canvas3fabric.calcOffset();
                                    var back = canvas3fabric.toDataURL('png');
                                    var $row = $('<div class="col-12 col-lg-6 mb-4"><img class="img-fluid img-back border border-primary" style="border-width: 5px !important;"></div>');
                                    $row.find('.img-back').attr('src', back);
                                    $('.images-result').append($row);

                                    deferred.resolve(
                                        a, //JSON.stringify(canvas2fabric.toJSON(["_controlsVisibility", "id"])),
                                        b === null ? a : b, //JSON.stringify(canvas3fabric.toJSON(["_controlsVisibility", "id"])),
                                        front,
                                        back
                                    );

                                    canvas2fabric.dispose();
                                    canvas3fabric.dispose();

                                    $.unblockUI();
                                }, 200);
                            },200);
                        }, 200);
                    },200);
                }, 200);
            } catch (e) {
            }
        }, 200);
    } catch (e) {
    }

    return deferred.promise();
}

function getRandomNum(min, max) {
    return Math.random() * (max - min) + min;
}

function onObjectClick(e) {
}

function onObjectSelected(e) {
    var selectedObject = e.target;
    if (!selectedObject) return;
    $("#text-string").val("");
    $('.add-text-button').removeClass('fa-plus fa-edit');
    $('.add-text-button').addClass('fa-plus');
    selectedObject.hasRotatingPoint = true
    if (selectedObject && isTextObject(selectedObject)) {
        //display text editor
        $("#texteditor").css('display', 'inline');
        $("#text-string").val(selectedObject.get('text'));
        $('#text-fontcolor').minicolors('value',selectedObject.get('fill'));
        $('#text-strokecolor').minicolors('value',selectedObject.get('stroke'));
        $("#imageeditor").css('display', 'inline');
        $('.add-text-button').removeClass('fa-plus');
        $('.add-text-button').addClass('fa-edit');
        addDeleteBtn(e.target.oCoords.tl.x, e.target.oCoords.tl.y);
    }
    else if (selectedObject && selectedObject.type === 'image'){
        //display image editor
        $("#texteditor").css('display', 'none');
        $("#imageeditor").css('display', 'inline');
        addDeleteBtn(e.target.oCoords.tl.x, e.target.oCoords.tl.y);
    }
}
function onSelectedCleared(){
    $("#texteditor").css('display', 'none');
    $("#text-string").val("");
    $("#imageeditor").css('display', 'none');
    $(".deleteBtn").remove();
}
function setFont(font){
    var activeObject = canvas.getActiveObject();
    if (activeObject && isTextObject(activeObject)) {
        activeObject.fontFamily = font;
        canvas.renderAll();
    }
}
function setFontSize(size){
    var activeObject = canvas.getActiveObject();
    if (activeObject && isTextObject(activeObject)) {
        activeObject.fontSize = size;
        canvas.renderAll();
    }
}
function setTextEffect(effect){
    var activeObject = canvas.getActiveObject();
    if (activeObject && isTextObject(activeObject)) {
        activeObject.toObject();
        var originalTop = activeObject.top;
        var originalLeft = activeObject.left;
        canvas.remove(activeObject);

        var textObject;

        if (effect === 'remove-effect') {
            textObject = new fabric.Text(activeObject.text, {
                textAlign: 'center',
                originX: 'center',
                originY: 'center',
                scaleX: activeObject.scaleX,
                scaleY: activeObject.scaleY,
                fontSize: activeObject.fontSize,
                fontFamily: activeObject.fontFamily,
                left: originalLeft,
                top: originalTop
            });

            textObject.setControlsVisibility(activeObject._controlsVisibility);
            textObject.set('fill', activeObject.get('fill'));
            textObject.set('stroke', activeObject.get('stroke'));
            textObject.set('strokeWidth', activeObject.get('strokeWidth'));
            textObject.set('strokeUniform', activeObject.get('strokeUniform'));
            textObject.set('hasRotatingPoint', activeObject.get('hasRotatingPoint'));

            textObject.setCoords();

            canvas.add(textObject);
            canvas.setActiveObject(textObject);
            canvas.renderAll();
        } else {
            var spacing = effect === 'bulge' ? 5 : 20;
            textObject = new fabric.CurvedText(activeObject.text, {
                textAlign: 'center',
                fontSize: activeObject.fontSize,
                radius: 150,
                spacing: spacing,
                originX: 'center',
                originY: 'center',
                scaleX: activeObject.scaleX,
                scaleY: activeObject.scaleY,
                fontFamily: activeObject.fontFamily,
                left: originalLeft,
                top: originalTop,
                effect: effect
            });

            textObject.setControlsVisibility(activeObject._controlsVisibility);
            textObject.set('fill', activeObject.get('fill'));
            textObject.set('stroke', activeObject.get('stroke'));
            textObject.set('strokeWidth', activeObject.get('strokeWidth'));
            textObject.set('strokeUniform', activeObject.get('strokeUniform'));
            textObject.set('hasRotatingPoint', activeObject.get('hasRotatingPoint'));

            textObject.setCoords();

            canvas.add(textObject);
            canvas.setActiveObject(textObject);
            canvas.renderAll();
        }
    }

    $(".deleteBtn").remove();

    addDeleteBtn(textObject.oCoords.tl.x, textObject.oCoords.tl.y);
    $("#texteditor").css('display', 'inline');
    $("#text-string").val(textObject.get('text'));
    $('#text-fontcolor').minicolors('value',textObject.get('fill'));
    $('#text-strokecolor').minicolors('value',textObject.get('stroke'));
    $('#text-strokewidth').val(textObject.get('strokeWidth'));
    $("#imageeditor").css('display', 'inline');
    $('.add-text-button').removeClass('fa-plus');
    $('.add-text-button').addClass('fa-edit');
}
function removeWhite(){
    var activeObject = canvas.getActiveObject();
    if (activeObject && activeObject.type === 'image') {
        activeObject.filters[2] =  new fabric.Image.filters.RemoveWhite({hreshold: 100, distance: 10});//0-255, 0-255
        activeObject.applyFilters(canvas.renderAll.bind(canvas));
    }
}
function addDeleteBtn(x, y){
    $(".deleteBtn").remove();
    var btnLeft = x-30;
    var btnTop = y-30;
    var deleteBtn = '<i class="deleteBtn fad fa-times-circle text-danger" style="position:absolute;top:'+btnTop+'px;left:'+btnLeft+'px;cursor:pointer;width:30px;height:30px;font-size:2rem;"></i>';
    $(".canvas-container").append(deleteBtn);
}

function rescale_canvas_if_needed(){
    var optimal_dimensions = [400*canvasRatio,800*canvasRatio];
    var scaleFactorX=window.innerWidth/optimal_dimensions[0];
    var scaleFactorY=window.innerHeight/optimal_dimensions[1];
    if(scaleFactorX <  scaleFactorY && scaleFactorX < 1) {
        canvas.setWidth(optimal_dimensions[0] *scaleFactorX);
        canvas.setHeight(optimal_dimensions[1] *scaleFactorX);
        canvas.setZoom(scaleFactorX);
    } else if(scaleFactorX >  scaleFactorY && scaleFactorY < 1){
        canvas.setWidth(optimal_dimensions[0] *scaleFactorY);
        canvas.setHeight(optimal_dimensions[1] *scaleFactorY);
        canvas.setZoom(scaleFactorY);
    }else {
        canvas.setWidth(optimal_dimensions[0] );
        canvas.setHeight(optimal_dimensions[1] );
        canvas.setZoom(1);
    }

    canvas.calcOffset();
    canvas.renderAll();
}

function handle_resize(){
    //$(".canvas-container").hide();
    //rescale_canvas_if_needed();
    //$(".canvas-container").show();
}

function isTextObject(object)
{
    return !object.hasOwnProperty('id') && (object.type === 'text' || object.type === 'curvedText')
}

function addImage(image)
{
    image.set({
        angle: 0,
        padding: 10,
        cornerRadius: 10,
        hasRotatingPoint:true
    });
    image.setControlsVisibility(HideControls);
    //image.scale(getRandomNum(0.1, 0.25)).setCoords();
    if (isSmallScreen()) {
        image.set('padding', 10 * canvasRatio);
        image.set('scaleX', canvasRatio);
        image.set('scaleY', canvasRatio);
        image.setCoords();
    }
    canvas.add(image);
    canvas.centerObject(image);
    image.setCoords();
    canvas.setActiveObject(image);
    addDeleteBtn(image.oCoords.tl.x, image.oCoords.tl.y);
    $("#imageeditor").css('display', 'inline');
    autoClosePaneIfSmallScreen();
}
fabric.Canvas.prototype.getItem = function(id) {
    var object = null,
        objects = this.getObjects();

    for (var i = 0, len = this.size(); i < len; i++) {
        if (objects[i].id && objects[i].id === id) {
            object = objects[i];
            break;
        }
    }

    return object;
};

function getDataUrl()
{
    window.open(canvas.toDataURL('png'));
}
