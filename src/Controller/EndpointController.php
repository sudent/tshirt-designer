<?php

namespace App\Controller;

use App\Entity\User;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EndpointController extends AbstractController
{
    /**
     * @Route("/endpoint/users/images", name="endpoint_user_images", options={"expose"=true})
     * @IsGranted("ROLE_USER")
     */
    public function index(CacheManager $cacheManager): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            return new JsonResponse([]);
        }

        $images = $user->getCustomImages();

        $datum = [];

        foreach ($images as $image) {
            $data = [
                'file_name' => $image->getOriginalName(),
                'url' => $cacheManager->generateUrl('uploads/' . $image->getFileName(), 'editor_image')
            ];

            $datum[] = $data;
        }

        return new JsonResponse($datum);
    }
}
