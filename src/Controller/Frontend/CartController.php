<?php

namespace App\Controller\Frontend;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Design;
use App\Entity\Order;
use App\Form\CartType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/cart", name="frontend_cart")
     */
    public function index(SessionInterface $session, Request $request): Response
    {
        $cart = null;

        if ($session->has('cart_uuid')) {
            $cartUuid = $session->get('cart_uuid');

            $cart = $this->getDoctrine()->getRepository(Cart::class)->findOneBy(['uuid' => $cartUuid]);
        }

        if (!$cart) {
            $cart = new Cart();
        }

        $form = $this->createForm(CartType::class, $cart);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('remove')->isClicked()) {
                if ($cart->getId() && $cart->getCartItems()->count() > 0) {
                    foreach ($cart->getCartItems() as $cartItem) {
                        $cart->removeCartItem($cartItem);
                        $this->getDoctrine()->getManager()->remove($cartItem);
                    }

                    $this->getDoctrine()->getManager()->remove($cart);
                    $this->getDoctrine()->getManager()->flush();
                }

                $session->remove('cart_uuid');

                $order = null;

                if ($session->has('order_uuid')) {
                    $orderUuid = $session->get('order_uuid');

                    $order = $this->getDoctrine()->getRepository(Order::class)->findOneBy([
                        'uuid' => $orderUuid
                    ]);

                    if ($order) {
                        foreach ($order->getOrderItems() as $orderItem) {
                            foreach ($orderItem->getOrderItemVariants() as $orderItemVariant) {
                                $orderItem->removeOrderItemVariant($orderItemVariant);
                                $this->getDoctrine()->getManager()->remove($orderItemVariant);
                            }

                            foreach ($orderItem->getOrderItemCustomNames() as $orderItemCustomName) {
                                $orderItem->removeOrderItemCustomName($orderItemCustomName);
                                $this->getDoctrine()->getManager()->remove($orderItemCustomName);
                            }

                            $order->removeOrderItem($orderItem);
                            $this->getDoctrine()->getManager()->remove($orderItem);
                        }

                        $orderShipping = $order->getOrderShipping();
                        $order->setOrderShipping(null);
                        $this->getDoctrine()->getManager()->remove($orderShipping);
                        $this->getDoctrine()->getManager()->remove($order);
                        $this->getDoctrine()->getManager()->flush();

                        $session->remove('order_uuid');
                    }
                }

                $this->addFlash('success', 'Cart have been successfully cleared.');

                return $this->redirectToRoute('frontend_cart');
            }

            $cart->recalculateTotal();

            $this->getDoctrine()->getManager()->persist($cart);
            $this->getDoctrine()->getManager()->flush();

            if ($form->get('save')->isClicked()) {
                $this->addFlash('success', 'Cart have been successfully updated!');
                return $this->redirectToRoute('frontend_cart');
            }

            if ($form->get('checkout')->isClicked()) {
                return $this->redirectToRoute('frontend_checkout');
            }
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'There is an error updating your cart. Please try again later.');
        }

        return $this->render('frontend/cart/index.html.twig', [
            'cart' => $cart,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/cart/add/{uuid}", name="frontend_add_design_to_cart")
     */
    public function add(Design $design, SessionInterface $session): Response
    {
        $cart = null;

        if ($session->has('cart_uuid')) {
            $cartUuid = $session->get('cart_uuid');

            $cart = $this->getDoctrine()->getRepository(Cart::class)->findOneBy(['uuid' => $cartUuid]);
        }

        if (!$cart) {
            $cart = new Cart();
        }

        $designAlreadyInCart = false;

        foreach ($cart->getCartItems() as $cartItem) {
            if ($cartItem->getDesign() === $design) {
                $cartItem->setQuantity($cartItem->getQuantity() + 1);
                $cartItem->setUnitPrice($design->getProduct()->getBasePrice() * 100);
                $cartItem->setTotal($cartItem->getUnitPrice() * $cartItem->getQuantity());
                $designAlreadyInCart = true;
            }
        }

        if (!$designAlreadyInCart) {
            $cartItem = new CartItem();
            $cartItem->setDesign($design);
            $cartItem->setUnitPrice($design->getProduct()->getBasePrice() * 100);
            $cartItem->setQuantity(1);
            $cartItem->setTotal($cartItem->getUnitPrice() * $cartItem->getQuantity());

            $cart->addCartItem($cartItem);
        }

        $cart->recalculateTotal();

        $this->getDoctrine()->getManager()->persist($cart);
        $this->getDoctrine()->getManager()->flush();

        $session->set('cart_uuid', $cart->getUuid());

        $this->addFlash('success', 'Your design have been added to cart.');

        return $this->redirectToRoute('frontend_cart');
    }
}
