<?php

namespace App\Controller\Frontend;

use App\Entity\CustomImage;
use App\Entity\Design;
use App\Entity\User;
use App\Form\ProfileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user/images", name="frontend_user_images")
     * @IsGranted("ROLE_USER")
     */
    public function index(): Response
    {
        $images = $this->getDoctrine()->getRepository(CustomImage::class)->findBy([
            'user' => $this->getUser()
        ], ['updatedAt' => 'DESC']);

        return $this->render('frontend/user/images.html.twig', [
            'images' => $images,
        ]);
    }

    /**
     * @Route("/user/images/{uuid}/delete", name="frontend_user_image_delete")
     * @IsGranted("ROLE_USER")
     */
    public function deleteImage(CustomImage $customImage): Response
    {
        $originalName = $customImage->getOriginalName();
        $fileName = $customImage->getFileName();

        $this->getDoctrine()->getManager()->remove($customImage);
        $this->getDoctrine()->getManager()->flush();

        $filesystem = new Filesystem();
        $filesystem->remove($this->getParameter('kernel.project_dir') .'/public/uploads/' . $fileName);

        $this->addFlash('success', '"' . $originalName . '" have been successfully removed.');

        return $this->redirectToRoute('frontend_user_images');
    }

    /**
     * @Route("/user/designs", name="frontend_user_designs")
     * @IsGranted("ROLE_USER")
     */
    public function designs(): Response
    {
        $designs = $this->getDoctrine()->getRepository(Design::class)->findBy([
            'user' => $this->getUser()
        ], ['updatedAt' => 'DESC']);

        return $this->render('frontend/user/designs.html.twig', [
            'designs' => $designs,
        ]);
    }

    /**
     * @Route("/user/profile", name="frontend_user_profile")
     * @IsGranted("ROLE_USER")
     */
    public function profile(Request $request, UserPasswordHasherInterface $userPasswordHasher): Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getId());

        if (!$user) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(ProfileType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('plainPassword')->getData() !== null) {
                $user->setPassword(
                    $userPasswordHasher->hashPassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );
            }

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Profile successfully updated!');
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Error saving your profile!');
        }

        return $this->render('frontend/user/profile.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
