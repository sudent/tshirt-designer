<?php

namespace App\Controller\Frontend;

use App\Datatables\UserOrderDatatable;
use App\Entity\Cart;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\OrderItemCustomName;
use App\Entity\OrderItemVariant;
use App\Entity\OrderPayment;
use App\Form\OrderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * @Route("/checkout", name="frontend_checkout")
     */
    public function index(SessionInterface $session, Request $request): Response
    {
        $cart = null;

        if ($session->has('cart_uuid')) {
            $cartUuid = $session->get('cart_uuid');

            $cart = $this->getDoctrine()->getRepository(Cart::class)->findOneBy(['uuid' => $cartUuid]);
        }

        if (!$cart || $cart->getCartItems()->count() === 0) {
            $this->addFlash('danger', 'Cannot checkout with an empty cart. Please try adding a product to the cart first.');

            return $this->redirectToRoute('frontend_cart');
        }

        $order = null;

        if ($session->has('order_uuid')) {
            $orderUuid = $session->get('order_uuid');

            $order = $this->getDoctrine()->getRepository(Order::class)->findOneBy([
                'uuid' => $orderUuid
            ]);
        }

        if (!$order) {
            $order = new Order();
        }

        $order->setTotal($cart->getTotal());

        foreach ($order->getOrderItems() as $orderItem) {
            $exists = false;

            foreach ($cart->getCartItems() as $cartItem) {
                if ($cartItem->getDesign() === $orderItem->getDesign()) {
                    $exists = true;
                    break;
                }
            }

            if (!$exists) {
                foreach ($orderItem->getOrderItemVariants() as $customVariant) {
                    $orderItem->removeOrderItemVariant($customVariant);
                    $this->getDoctrine()->getManager()->remove($customVariant);
                }

                foreach ($orderItem->getOrderItemCustomNames() as $customName) {
                    $orderItem->removeOrderItemCustomName($customName);
                    $this->getDoctrine()->getManager()->remove($customName);
                }

                $order->removeOrderItem($orderItem);
                $this->getDoctrine()->getManager()->remove($orderItem);
            }
        }

        foreach ($cart->getCartItems() as $cartItem) {
            $orderItem = null;

            foreach ($order->getOrderItems() as $existingOrderItem) {
                if ($existingOrderItem->getDesign() === $cartItem->getDesign()) {
                    $orderItem = $existingOrderItem;
                    break;
                }
            }

            $design = $cartItem->getDesign();

            if (!$orderItem) {
                $orderItem = new OrderItem();
                $orderItem->setDesign($design);
                $orderItem->setProduct($design->getProduct());
            }

            $orderItem->setUnitPrice($cartItem->getUnitPrice());
            $orderItem->setQuantity($cartItem->getQuantity());
            $orderItem->setTotal($cartItem->getTotal());

            if ($design->getHasNames() && $orderItem->getOrderItemCustomNames()->count() === 0) {
                $orderItemCustomName = new OrderItemCustomName();
                $orderItem->addOrderItemCustomName($orderItemCustomName);
            }

            if ($orderItem->getOrderItemVariants()->count() === 0) {
                $orderItemVariant = new OrderItemVariant();
                $orderItemVariant->setQuantity($orderItem->getQuantity());
                $orderItemVariant->setType('adults');
                $orderItemVariant->setSize('m');
                $orderItemVariant->setUnitPrice($orderItem->getUnitPrice());
                $orderItem->addOrderItemVariant($orderItemVariant);
            }

            $order->addOrderItem($orderItem);
        }

        $form = $this->createForm(OrderType::class, $order);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $order->recalculateTotal();

            $MIN_SESSION_ID = 1000000000;
            $MAX_SESSION_ID = 9999999999;
            $randomNumber = random_int($MIN_SESSION_ID, $MAX_SESSION_ID);

            $existingOrder = $this->getDoctrine()->getRepository(Order::class)->findOneBy([
                'number' => $randomNumber
            ]);

            while ($existingOrder !== null) {
                $randomNumber = random_int($MIN_SESSION_ID, $MAX_SESSION_ID);

                $existingOrder = $this->getDoctrine()->getRepository(Order::class)->findOneBy([
                    'number' => $randomNumber
                ]);
            }

            $order->setNumber($randomNumber);

            if ($this->getUser()) {
                $order->setUser($this->getUser());
            }

            $this->getDoctrine()->getManager()->persist($order);
            $this->getDoctrine()->getManager()->flush();

            $session->set('order_uuid', $order->getUuid());

            return $this->redirectToRoute('frontend_checkout_finalize');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'There is an error with your order details. Please try again.');
        }

        return $this->render('frontend/order/index.html.twig', [
            'form' => $form->createView(),
            'payment_step' => false
        ]);
    }

    /**
     * @Route("/checkout/finalize", name="frontend_checkout_finalize")
     */
    public function finalize(Request $request, SessionInterface $session): Response
    {
        $order = null;

        if ($session->has('order_uuid')) {
            $orderUuid = $session->get('order_uuid');

            $order = $this->getDoctrine()->getRepository(Order::class)->findOneBy([
                'uuid' => $orderUuid
            ]);
        }

        if (!$order) {
            $this->addFlash('danger', 'Cannot retrieve order details. Please try again.');
            return $this->redirectToRoute('frontend_cart');
        }

        $form = $this->createFormBuilder();
        $form->add('submit', SubmitType::class);
        $form = $form->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $orderPayment = new OrderPayment();
            $orderPayment->setTotal($order->getTotal());
            $orderPayment->setPaymentMethodCode('fpx');
            $orderPayment->setState('paid');

            $MIN_SESSION_ID = 1000000000;
            $MAX_SESSION_ID = 9999999999;
            $randomNumber = random_int($MIN_SESSION_ID, $MAX_SESSION_ID);

            $orderPayment->setReferenceNumber($randomNumber);

            if ($this->getUser()) {
                $orderPayment->setUser($this->getUser());
            }

            $order->addOrderPayment($orderPayment);
            $order->setPaidOrderPayment($orderPayment);

            $order->setState('paid');

            $this->getDoctrine()->getManager()->persist($order);
            $this->getDoctrine()->getManager()->flush();

            if ($session->has('order_uuid')) {
                $session->remove('order_uuid');
            }

            if ($session->has('cart_uuid')) {
                $session->remove('cart_uuid');
            }

            if (!$this->getUser()) {
                if ($session->has('orders')) {
                    $orders = $session->get('orders');
                } else {
                    $orders = [];
                }

                $orders[] = $order->getUuid();

                $session->set('orders', $orders);
            }

            $this->addFlash('success', 'Successfully placed order for the products! We will emailed you back if we have any updates, stay tune!');

            return $this->redirectToRoute('frontend_order_receipt', ['uuid' => $order->getUuid()]);
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Error finalizing the order. Please try again.');
        }

        return $this->render('frontend/order/finalize.html.twig', [
            'form' => $form->createView(),
            'order' => $order
        ]);
    }

    /**
     * @Route("/orders/{uuid}/receipt", name="frontend_order_receipt")
     */
    public function receipt(Order $order): Response
    {
        if ($order->getUser() && $order->getUser() !== $this->getUser()) {
            throw $this->createAccessDeniedException();
        }

        return $this->render('frontend/order/receipt.html.twig', [
            'order' => $order
        ]);
    }

    /**
     * @Route("/user/orders", name="frontend_user_orders_index", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function userOrders(Request $request, DatatableFactory $datatableFactory, DatatableResponse $datatableResponse): Response
    {
        $isAjax = $request->isXmlHttpRequest();

        $datatable = $datatableFactory->create(UserOrderDatatable::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $datatableResponse->setDatatable($datatable);

            $datatableQueryBuilder = $datatableResponse->getDatatableQueryBuilder();
            $orderRepository = $this->getDoctrine()->getRepository(Order::class);
            $orderRepository->getUserOrders($datatableQueryBuilder->getQb(), $this->getUser());

            return $datatableResponse->getResponse();
        }

        return $this->render('frontend/order/orders.html.twig', array(
            'datatable' => $datatable,
        ));
    }
}
