<?php

namespace App\Controller\Frontend;

use App\Entity\Art;
use App\Entity\Design;
use App\Entity\Product;
use App\Form\DesignType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="frontend_index")
     */
    public function index(Request $request, SessionInterface $session): Response
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findBy([
            'enabled' => true
        ]);

        $arts = $this->getDoctrine()->getRepository(Art::class)->findBy([
            'enabled' => true
        ]);

        $design = new Design();
        $design->setBackgroundColor('#FFFFFF');
        $design->setNamesSide('back');
        $design->setNamesHeight(2);
        $design->setNamesColor('#000000');
        $design->setNumbersSide('back');
        $design->setNumbersHeight(8);
        $design->setNumbersColor('#000000');

        $form = $this->createForm(DesignType::class, $design);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->getUser() && !$design->getUser()) {
                $design->setUser($this->getUser());
            }

            $this->getDoctrine()->getManager()->persist($design);
            $this->getDoctrine()->getManager()->flush();

            if (!$design->getUser()) {
                if ($session->has('designs')) {
                    $designsSession = $session->get('designs');
                } else {
                    $designsSession = [];
                }

                $designsSession[] = $design->getUuid();
                $session->set('designs', $designsSession);
            }

            if ($form->get('action')->getData() === 'save') {
                $this->addFlash('success', 'Your design have been saved.');
                return $this->redirectToRoute('frontend_load_design', ['uuid' => $design->getUuid()]);
            }

            if ($form->get('action')->getData() === 'add-to-cart') {
                return $this->redirectToRoute('frontend_add_design_to_cart', ['uuid' => $design->getUuid()]);
            }
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'There is an error processing your design. Please try again.');
        }

//        if ($design->getDesignNames()->count() !== $design->getDesignNumbers()->count()) {
//            if ($design->getDesignNames()->count() > $design->getDesignNumbers()->count()) {
//                while ($design->getDesignNumbers()->count() < $design->getDesignNames()->count()) {
//                    $design->addDesignNumber(new DesignNumber());
//                }
//            } else {
//                while ($design->getDesignNames()->count() <= $design->getDesignNumbers()->count()) {
//                    $design->addDesignName(new DesignName());
//                }
//            }
//        }

//        $index = 0;
//        $namesCount = 0;
//        $numbersCount = 0;
//
//        if (count($design->getDesignNames()) > 0 || count($design->getDesignNumbers()) > 0) {
//            $namesCount = count($design->getDesignNames());
//            $numbersCount = count($design->getDesignNumbers());
//
//            if ($namesCount > 0 && $namesCount >= $numbersCount) {
//                $index = $namesCount;
//            } else if ($numbersCount > 0 && $numbersCount >= $namesCount){
//                $index = $numbersCount;
//            }
//        }

        return $this->render('frontend/index/index.html.twig', [
            'products' => $products,
            'arts' => $arts,
            'form' => $form->createView(),
            //'index' => $index,
            'design' => $design,
            //'names_count' => $namesCount,
            //'numbers_count' => $numbersCount
        ]);
    }

    /**
     * @Route("/design/{uuid}/claim", name="frontend_claim_design")
     * @IsGranted("ROLE_USER")
     */
    public function claimDesign(Design $design): Response
    {
        if (!$design->getProduct()->isEnabled()) {
            $this->addFlash('danger', 'The product for this design is unavailable currently. As a result, you
            can\'t customized the saved design for the time being.');

            return $this->redirectToRoute('frontend_index');
        }

        if ($design->getUser()) {
            $this->addFlash('danger', 'This design have already been claimed by ' . $design->getUser()->getName());
        } else {
            $design->setUser($this->getUser());
            $this->getDoctrine()->getManager()->persist($design);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Successfully claimed the design as yours. REJOICE IN THE GLORY OF COMBAT!');
        }

        return $this->redirectToRoute('frontend_load_design', ['uuid' => $design->getUuid()]);
    }

    /**
     * @Route("/design/{uuid}/ogimage", name="frontend_generate_ogimage")
     */
    public function ogImage(Design $design): Response
    {
        if ($design->getFrontSideThumbnail() !== null) {
            $image = stream_get_contents($design->getFrontSideThumbnail());
            $image = str_replace('data:image/png;base64,', '', $image);

            $response = new Response(base64_decode($image));
            $response->headers->set('Content-Type', 'image/png');

            return $response;
        }

        throw $this->createNotFoundException();
    }

    /**
     * @Route("/design/{uuid}", name="frontend_load_design")
     */
    public function loadDesign(Request $request, Design $design, SessionInterface $session): Response
    {
        if (!$design->getProduct()->isEnabled()) {
            $this->addFlash('danger', 'The product for this design is unavailable currently. As a result, you
            can\'t customized the saved design for the time being.');

            return $this->redirectToRoute('frontend_index');
        }

        $products = $this->getDoctrine()->getRepository(Product::class)->findBy([
            'enabled' => true
        ]);

        $arts = $this->getDoctrine()->getRepository(Art::class)->findBy([
            'enabled' => true
        ]);

        $form = $this->createForm(DesignType::class, $design);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->getUser() && !$design->getUser()) {
                $design->setUser($this->getUser());
            }

            $this->getDoctrine()->getManager()->persist($design);
            $this->getDoctrine()->getManager()->flush();

            if (!$design->getUser()) {
                if ($session->has('designs')) {
                    $designsSession = $session->get('designs');
                } else {
                    $designsSession = [];
                }

                if (!in_array($design->getUuid(), $designsSession, true)) {
                    $designsSession[] = $design->getUuid();
                    $session->set('designs', $designsSession);
                }
            }

            if ($form->get('action')->getData() === 'save') {
                $this->addFlash('success', 'Your design have been saved.');
                return $this->redirectToRoute('frontend_load_design', ['uuid' => $design->getUuid()]);
            }

            if ($form->get('action')->getData() === 'add-to-cart') {
                return $this->redirectToRoute('frontend_add_design_to_cart', ['uuid' => $design->getUuid()]);
            }
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'There is an error processing your design. Please try again.');
        }

        return $this->render('frontend/index/index.html.twig', [
            'products' => $products,
            'arts' => $arts,
            'form' => $form->createView(),
            'design' => $design
        ]);
    }

    /**
     * @Route("/about", name="frontend_about")
     */
    public function about(): Response
    {
        return $this->render('frontend/index/about.html.twig');
    }
}
