<?php

namespace App\Controller;

use App\Entity\CustomImage;
use App\Entity\Design;
use App\Entity\Order;
use App\Entity\User;
use App\Form\SignupType;
use App\Security\LoginFormAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/signup", name="app_signup")
     */
    public function signup(Request $request, UserPasswordHasherInterface $userPasswordHasherInterface, LoginFormAuthenticator $loginFormAuthenticator, UserAuthenticatorInterface $userAuthenticator, SessionInterface $session): Response
    {
        $user = new User();

        $form = $this->createForm(SignupType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $userPasswordHasherInterface->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'You have successfully signed up! You can start designing your own T-Shirt right away with additional features.');

            if ($session->has('designs')) {
                $designsUuid = $session->get('designs');

                foreach ($designsUuid as $uuid) {
                    $design = $this->getDoctrine()->getRepository(Design::class)->findOneBy([
                        'uuid' => $uuid
                    ]);

                    if ($design && !$design->getUser()) {
                        $design->setUser($user);
                        $this->getDoctrine()->getManager()->persist($design);
                    }
                }

                $this->getDoctrine()->getManager()->flush();

                $session->remove('designs');
            }

            if ($session->has('custom_images')) {
                $customImagesId = $session->get('custom_images');

                foreach ($customImagesId as $customImageId) {
                    $customImage = $this->getDoctrine()->getRepository(CustomImage::class)->find($customImageId);

                    if ($customImage && !$customImage->getUser()) {
                        $customImage->setUser($user);
                        $this->getDoctrine()->getManager()->persist($customImage);
                    }
                }

                $this->getDoctrine()->getManager()->flush();

                $session->remove('custom_images');
            }

            if ($session->has('orders')) {
                $ordersUuid = $session->get('orders');

                foreach ($ordersUuid as $orderUuid) {
                    $order = $this->getDoctrine()->getRepository(Order::class)->findOneBy([
                        'uuid' => $orderUuid
                    ]);

                    if ($order && !$order->getUser()) {
                        $order->setUser($user);
                        $this->getDoctrine()->getManager()->persist($order);
                    }
                }

                $this->getDoctrine()->getManager()->flush();

                $session->remove('orders');
            }

            return $userAuthenticator->authenticateUser($user, $loginFormAuthenticator, $request);
        }

        return $this->render('security/signup.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
