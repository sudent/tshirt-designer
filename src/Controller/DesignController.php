<?php

namespace App\Controller;

use App\Entity\Design;
use App\Form\DesignType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DesignController extends AbstractController
{
    /**
     * @Route("/design/save", name="save_design")
     */
    public function index(Request $request): Response
    {
        $design = new Design();

        $form = $this->createForm(DesignType::class, $design);

        $form->handleRequest($request);

        $errors = [];

        if ($form->isSubmitted() && $form->isValid()) {

        } elseif ($form->isSubmitted() && !$form->isValid()) {
            foreach ($form->all() as $child) {
                foreach ($child->getErrors() as $error) {
                    $name = $child->getName();
                    $errors[$name] = $error->getMessage();
                }
            }
        }

        $response = new JsonResponse(['status' => 'FAILED', 'errors' => $errors]);

        return $response;
    }
}
