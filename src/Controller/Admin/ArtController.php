<?php

namespace App\Controller\Admin;

use App\Datatables\ArtDatatable;
use App\Entity\Art;
use App\Form\Admin\ArtType;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/art")
 */
class ArtController extends AbstractController
{
    /**
     * @Route("/", name="admin_art_index", methods={"GET"})
     */
    public function index(Request $request, DatatableFactory $datatableFactory, DatatableResponse $datatableResponse): Response
    {
        $isAjax = $request->isXmlHttpRequest();

        $datatable = $datatableFactory->create(ArtDatatable::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $datatableResponse->setDatatable($datatable);
            $datatableResponse->getDatatableQueryBuilder();

            return $datatableResponse->getResponse();
        }

        return $this->render('admin/art/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/new", name="admin_art_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $art = new Art();
        $form = $this->createForm(ArtType::class, $art, ['validation_groups' => ['Default', 'new']]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($art);
            $entityManager->flush();

            return $this->redirectToRoute('admin_art_index');
        }

        return $this->render('admin/art/new.html.twig', [
            'art' => $art,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_art_show", methods={"GET"})
     */
    public function show(Art $art): Response
    {
        return $this->render('admin/art/show.html.twig', [
            'art' => $art,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_art_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Art $art): Response
    {
        $form = $this->createForm(ArtType::class, $art);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_art_index');
        }

        return $this->render('admin/art/edit.html.twig', [
            'art' => $art,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_art_delete", methods={"POST"})
     */
    public function delete(Request $request, Art $art): Response
    {
        if ($this->isCsrfTokenValid('delete'.$art->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($art);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_art_index');
    }
}
