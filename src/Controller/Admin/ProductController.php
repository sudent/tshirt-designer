<?php

namespace App\Controller\Admin;

use App\Datatables\ProductDatatable;
use App\Entity\Product;
use App\Form\Admin\ProductType;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/product")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="admin_product_index", methods={"GET"})
     */
    public function index(Request $request, DatatableFactory $datatableFactory, DatatableResponse $datatableResponse): Response
    {
        $isAjax = $request->isXmlHttpRequest();

        $datatable = $datatableFactory->create(ProductDatatable::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $datatableResponse->setDatatable($datatable);
            $datatableResponse->getDatatableQueryBuilder();

            return $datatableResponse->getResponse();
        }

        return $this->render('admin/product/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/new", name="admin_product_new", methods={"GET","POST"})
     */
    public function new(Request $request, $projectDir): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product, ['validation_groups' => ['Default', 'new']]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $frontSideFileName = $projectDir . '/public/assets/img/crew_front.png';
            $frontTempFile = tempnam(sys_get_temp_dir(), 'FRONT_');
            file_put_contents($frontTempFile, file_get_contents($frontSideFileName));
            $frontSideFile = new UploadedFile($frontTempFile, 'crew_front.png', filesize($frontTempFile), false, true);
            $product->setFrontSideImageFile($frontSideFile);

            $backSideFileName = $projectDir . '/public/assets/img/crew_back.png';
            $backTempFile = tempnam(sys_get_temp_dir(), 'BACK_');
            file_put_contents($backTempFile, file_get_contents($backSideFileName));
            $backSideFile = new UploadedFile($backTempFile, 'crew_back.png', filesize($backTempFile), false, true);
            $product->setBackSideImageFile($backSideFile);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            $this->addFlash('success', 'Product updated.');

            return $this->redirectToRoute('admin_product_index');
        }

        return $this->render('admin/product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_product_show", methods={"GET"})
     */
    public function show(Product $product): Response
    {
        return $this->render('admin/product/show.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_product_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Product $product): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Product updated.');

            return $this->redirectToRoute('admin_product_index');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'There is an error with your form.');
        }

        return $this->render('admin/product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_product_delete", methods={"POST"})
     */
    public function delete(Request $request, Product $product): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_product_index');
    }
}
