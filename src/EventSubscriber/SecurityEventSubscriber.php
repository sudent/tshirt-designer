<?php

namespace App\EventSubscriber;

use App\Entity\CustomImage;
use App\Entity\Design;
use App\Entity\Order;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;

class SecurityEventSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $em;

    private SessionInterface $session;

    public function __construct(EntityManagerInterface $em, SessionInterface $session)
    {
        $this->em = $em;
        $this->session = $session;
    }

    public function onLoginSuccess(LoginSuccessEvent $event)
    {
        /** @var User $user */
        $user = $event->getUser();

        // Assign custom images and designs to the just logged in user

        if ($this->session->has('designs')) {
            $designsUuid = $this->session->get('designs');

            foreach ($designsUuid as $uuid) {
                $design = $this->em->getRepository(Design::class)->findOneBy([
                    'uuid' => $uuid
                ]);

                if ($design && !$design->getUser()) {
                    $design->setUser($user);
                    $this->em->persist($design);
                }
            }

            $this->em->flush();

            $this->session->remove('designs');
        }

        if ($this->session->has('custom_images')) {
            $customImagesId = $this->session->get('custom_images');

            foreach ($customImagesId as $customImageId) {
                $customImage = $this->em->getRepository(CustomImage::class)->find($customImageId);

                if ($customImage && !$customImage->getUser()) {
                    $customImage->setUser($user);
                    $this->em->persist($customImage);
                }
            }

            $this->em->flush();

            $this->session->remove('custom_images');
        }

        if ($this->session->has('orders')) {
            $ordersUuid = $this->session->get('orders');

            foreach ($ordersUuid as $orderUuid) {
                $order = $this->em->getRepository(Order::class)->findOneBy(['uuid' => $orderUuid]);

                if ($order && !$order->getUser()) {
                    $order->setUser($user);
                    $this->em->persist($order);
                }
            }

            $this->em->flush();

            $this->session->remove('orders');
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            LoginSuccessEvent::class => 'onLoginSuccess',
        ];
    }
}
