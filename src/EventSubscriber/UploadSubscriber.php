<?php

namespace App\EventSubscriber;

use App\Entity\CustomImage;
use Doctrine\ORM\EntityManagerInterface;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Oneup\UploaderBundle\Event\PostUploadEvent;
use Oneup\UploaderBundle\Event\PreUploadEvent;
use Oneup\UploaderBundle\UploadEvents;
use Symfony\Component\Asset\PackageInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

class UploadSubscriber implements EventSubscriberInterface
{
    /**
     * @var PackageInterface
     */
    private $package;

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $originalFileName;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(PackageInterface $package, CacheManager $cacheManager, Security $security, EntityManagerInterface $entityManager, RequestStack $requestStack)
    {
        $this->package = $package;
        $this->cacheManager = $cacheManager;
        $this->security = $security;
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
    }

    public static function getSubscribedEvents()
    {
        return [
            UploadEvents::preUpload('gallery') => ['onPreUpload'],
            UploadEvents::postUpload('gallery') => ['onPostUpload']
        ];
    }

    public function onPreUpload(PreUploadEvent $event)
    {
        $file = $event->getFile();
        $this->originalFileName = $file->getClientOriginalName();
    }

    public function onPostUpload(PostUploadEvent $event)
    {
        $file = $event->getFile();

        $customImage = new CustomImage();
        $customImage->setFileName($file->getFilename());

        if ($user = $this->security->getUser()) {
            $customImage->setUser($user);
        }

        $customImage->setOriginalName($this->originalFileName);

        $this->entityManager->persist($customImage);
        $this->entityManager->flush();

         if (!$user) {
            $session = $this->requestStack->getSession();

            if ($session->has('custom_images')) {
                $customImages = $session->get('custom_images');
            } else {
                $customImages = [];
            }

            $customImages[] = $customImage->getId();

            $session->set('custom_images', $customImages);
        }

        $response = $event->getResponse();

        $response['src'] = $this->cacheManager->generateUrl(
            'uploads/' . $file->getFilename(),
            'editor_image'
        );

        $response['uuid'] = $customImage->getUuid();
    }

    public function setCacheManager(CacheManager $cacheManager)
    {
        $this->cacheManager = $cacheManager;
    }
}
