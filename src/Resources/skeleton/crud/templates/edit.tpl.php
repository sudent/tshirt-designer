{% extends 'base.html.twig' %}

{% block title %}Edit <?= $entity_class_name ?>{% endblock %}

{% block content %}
    <h4 class="font-weight-bold py-3 mb-4">Edit <?= $entity_class_name ?> "{{ <?= $entity_twig_var_singular ?> }}"</h4>

    {{ include('<?= $templates_path ?>/_form.html.twig', {'button_label': 'Update'}) }}

    {{ include('<?= $templates_path ?>/_delete_form.html.twig') }}
{% endblock %}
