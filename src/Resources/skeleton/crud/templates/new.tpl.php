{% extends 'base.html.twig' %}

{% block title %}New <?= $entity_class_name ?>{% endblock %}

{% block content %}
    <h4 class="font-weight-bold py-3 mb-4">Create New <?= $entity_class_name ?></h4>

    {{ include('<?= $templates_path ?>/_form.html.twig') }}
{% endblock %}
