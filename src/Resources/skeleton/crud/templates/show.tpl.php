{% extends 'base.html.twig' %}

{% block title %}View <?= $entity_class_name ?>{% endblock %}

{% block content %}
    <h4 class="font-weight-bold py-3 mb-4">View <?= $entity_class_name ?> "{{ <?= $entity_twig_var_singular ?> }}"</h4>

    <table class="table">
        <tbody>
<?php foreach ($entity_fields as $field): ?>
            <tr>
                <th><?= ucfirst($field['fieldName']) ?></th>
                <td>{{ <?= $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}</td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>

    <a href="{{ path('<?= $route_name ?>_index') }}" class="btn btn-secondary">Back to List</a>

    <a href="{{ path('<?= $route_name ?>_edit', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}" class="btn btn-primary">Edit</a>

    <button class="btn btn-danger" form="<?= $route_name ?>_delete_form">Delete</button>
    {{ include('<?= $templates_path ?>/_delete_form.html.twig') }}
{% endblock %}
