<?php

namespace App\Form\DataTransformer;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ProductToCodeTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  Product|null $product
     */
    public function transform($product): string
    {
        if (null === $product) {
            return '';
        }

        return $product->getCode();
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $productCode
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($productCode): ?Product
    {
        // no issue number? It's optional, so that's ok
        if (!$productCode) {
            return null;
        }

        /** @var Product $product */
        $product = $this->entityManager
            ->getRepository(Product::class)
            // query for the issue with this id
            ->findOneBy(['code' => $productCode])
        ;

        if (null === $product) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A product with code "%s" does not exist!',
                $productCode
            ));
        }

        return $product;
    }
}
