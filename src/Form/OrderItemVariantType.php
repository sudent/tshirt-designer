<?php

namespace App\Form;

use App\Entity\OrderItemVariant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderItemVariantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Adults' => 'adults',
                    'Kids' => 'kids',
                    'Youths' => 'youths'
                ]
            ])
            ->add('size', ChoiceType::class, [
                'choices' => [
                    'S' => 's',
                    'M' => 'm',
                    'L' => 'l',
                    'XL' => 'xl'
                ]
            ])
            ->add('quantity', IntegerType::class, ['attr' => ['min' => 1, 'class' => 'variant-quantity']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OrderItemVariant::class,
        ]);
    }
}
