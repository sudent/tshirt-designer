<?php

namespace App\Form\Admin;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Currencies;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        \Locale::setDefault('en');

        $builder
            ->add('code')
            ->add('name')
            ->add('description')
            ->add('basePrice', MoneyType::class, [
                'currency' => Currencies::getSymbol('MYR')
            ])
            ->add('enabled')
            ->add('image', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_label' => 'Download',
                'download_uri' => true,
                'image_uri' => true,
                'asset_helper' => true,
                'label' => 'Product Image'
            ])
            /*->add('frontSideImageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_label' => 'Download',
                'download_uri' => true,
                'image_uri' => true,
                'asset_helper' => true,
                'label' => 'Template Front Side Image'
            ])
            ->add('backSideImageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_label' => 'Download',
                'download_uri' => true,
                'image_uri' => true,
                'asset_helper' => true,
                'label' => 'Template Back Side Image'
            ])*/
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
