<?php

namespace App\Form\Admin;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserType extends AbstractType
{
    private TokenStorageInterface $tokenStorage;
    private AuthorizationCheckerInterface $authorizationChecker;

    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')
            ->add('plainPassword', PasswordType::class, [
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'label' => 'Password'
            ])
            ->add('name')
        ;

        $tokenStorage = $this->tokenStorage;
        $authorizationChecker = $this->authorizationChecker;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($tokenStorage, $authorizationChecker) {
            $form = $event->getForm();
            /** @var User $user */
            $user = $event->getData();

            $choices = [];

            $choices['Admin'] = User::ROLE_ADMIN;
            $choices['User'] = User::ROLE_USER;

            $form->add('role', ChoiceType::class, [
                'label' => 'User Role',
                'expanded' => true,
                'multiple' => false,
                'choices' => $choices,
                'attr' => ['class' => 'custom-controls-stacked'],
                'label_attr' => ['class' => 'radio-custom'],
                // Disable editing role for current user
                'disabled' => $user === $tokenStorage->getToken()->getUser(),
                'help' => $user === $tokenStorage->getToken()->getUser() ? 'Unable to change role for your own account' : null,
                'required' => true
            ]);

            $form->add('enabled', null, [
                'label' => 'Account Enabled?',
                // Disable editing status for current user
                'disabled' => $user === $tokenStorage->getToken()->getUser(),
                'label_attr' => ['class' => 'checkbox-custom'],
            ]);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
