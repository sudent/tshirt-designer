<?php

namespace App\Form\Admin;

use App\Entity\Order;
use App\Form\OrderShippingType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('number')
            ->add('state')
            ->add('itemsTotal')
            ->add('shippingTotal')
            ->add('total')
            ->add('notes')
            ->add('orderShipping', OrderShippingType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
