<?php

namespace App\Form;

use App\Entity\OrderItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('quantity', IntegerType::class, ['attr' => ['min' => 1, 'class' => 'order-item-quantity']])
            ->add('orderItemCustomNames', CollectionType::class, [
                'entry_type' => OrderItemCustomNameType::class,
                'entry_options' => ['label' => false],
                'allow_delete' => true,
                'by_reference' => false,
                'allow_add' => true,
            ])
            ->add('orderItemVariants', CollectionType::class, [
                'entry_type' => OrderItemVariantType::class,
                'entry_options' => ['label' => false],
                'allow_delete' => true,
                'by_reference' => false,
                'allow_add' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OrderItem::class,
        ]);
    }
}
