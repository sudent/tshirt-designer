<?php

namespace App\Form;

use App\Entity\Design;
use App\Form\DataTransformer\ProductToCodeTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DesignType extends AbstractType
{
    private $transformer;

    public function __construct(ProductToCodeTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('backgroundColor', null, [
                'attr' => [
                    'class' => 'minicolors'
                ]
            ])
            ->add('frontSideJsonData', HiddenType::class)
            ->add('backSideJsonData', HiddenType::class)
            ->add('hasNames', null, [
                'attr' => [
                    'class' => 'add-names'
                ],
                'label' => 'Add Names',
                'label_attr' => ['class' => 'checkbox-custom']
            ])
            ->add('namesSide', ChoiceType::class, [
                'attr' => [
                    'class' => 'names side',
                    'data-original-side' => 'back'
                ],
                'choices' => [
                    'Back' => 'back',
                    'Front' => 'front'
                ]
            ])
            ->add('namesHeight', ChoiceType::class, [
                'attr' => [
                    'class' => 'names height'
                ],
                'choices' => [
                    '1 In' => 1,
                    '2 In' => 2
                ]
            ])
            ->add('namesColor', null, [
                'attr' => [
                    'class' => 'names minicolors'
                ]
            ])
            ->add('hasNumbers', null, [
                'attr' => [
                    'class' => 'add-numbers'
                ],
                'label' => 'Add Numbers',
                'label_attr' => ['class' => 'checkbox-custom']
            ])
            ->add('numbersSide', ChoiceType::class, [
                'attr' => [
                    'class' => 'numbers side',
                    'data-original-side' => 'back'
                ],
                'choices' => [
                    'Back' => 'back',
                    'Front' => 'front'
                ]
            ])
            ->add('numbersHeight', ChoiceType::class, [
                'attr' => [
                    'class' => 'numbers height'
                ],
                'choices' => [
                    '2 In' => 2,
                    '4 In' => 4,
                    '6 In' => 6,
                    '8 In' => 8
                ]
            ])
            ->add('numbersColor', null, [
                'attr' => [
                    'class' => 'numbers minicolors'
                ]
            ])
            ->add('frontSideThumbnail', HiddenType::class)
            ->add('backSideThumbnail', HiddenType::class)
            ->add('product', HiddenType::class)
//            ->add('designNames', CollectionType::class, [
//                'entry_type' => DesignNameType::class,
//                'entry_options' => ['label' => false],
//                'allow_delete' => true,
//                'by_reference' => false,
//                'allow_add' => true,
//            ])
//            ->add('designNumbers', CollectionType::class, [
//                'entry_type' => DesignNumberType::class,
//                'entry_options' => ['label' => false],
//                'allow_delete' => true,
//                'by_reference' => false,
//                'allow_add' => true,
//            ])
            ->add('action', HiddenType::class, ['mapped' => false])
            ->add('originalWidth', HiddenType::class)
            ->add('originalHeight', HiddenType::class)
        ;

        $builder->get('frontSideJsonData')
            ->addModelTransformer(new CallbackTransformer(
                function ($valueAsArray) {
                    // transform the array to a string
                    return json_encode($valueAsArray);
                },
                function ($valueAsString) {
                    // transform the string back to an array
                    return json_decode($valueAsString, true);
                }
            ))
        ;

        $builder->get('backSideJsonData')
            ->addModelTransformer(new CallbackTransformer(
                function ($valueAsArray) {
                    // transform the array to a string
                    return json_encode($valueAsArray);
                },
                function ($valueAsString) {
                    // transform the string back to an array
                    return json_decode($valueAsString, true);
                }
            ))
        ;

        $builder->get('product')
            ->addModelTransformer($this->transformer);



        $builder->addEventListener(
            FormEvents::SUBMIT,
            function(FormEvent $event) use ($options) {
                /** @var Design $data */
                $data = $event->getData();
                $form = $event->getForm();
                $namesDisabled = !$data->getHasNames();
                $numbersDisabled = !$data->getHasNumbers();

                $form
                    ->add('namesSide', ChoiceType::class, [
                        'attr' => [
                            'class' => 'names side',
                            'data-original-side' => 'back'
                        ],
                        'choices' => [
                            'Back' => 'back',
                            'Front' => 'front'
                        ],
                        'disabled' => $namesDisabled
                    ])
                    ->add('namesHeight', ChoiceType::class, [
                        'attr' => [
                            'class' => 'names height'
                        ],
                        'choices' => [
                            '1 In' => 1,
                            '2 In' => 2
                        ],
                        'disabled' => $namesDisabled
                    ])
                    ->add('namesColor', null, [
                        'attr' => [
                            'class' => 'names minicolors'
                        ],
                        'disabled' => $namesDisabled
                    ])
                    ->add('numbersSide', ChoiceType::class, [
                        'attr' => [
                            'class' => 'numbers side',
                            'data-original-side' => 'back'
                        ],
                        'choices' => [
                            'Back' => 'back',
                            'Front' => 'front'
                        ],
                        'disabled' => $numbersDisabled
                    ])
                    ->add('numbersHeight', ChoiceType::class, [
                        'attr' => [
                            'class' => 'numbers height'
                        ],
                        'choices' => [
                            '2 In' => 2,
                            '4 In' => 4,
                            '6 In' => 6,
                            '8 In' => 8
                        ],
                        'disabled' => $numbersDisabled
                    ])
                    ->add('numbersColor', null, [
                        'attr' => [
                            'class' => 'numbers minicolors'
                        ],
                        'disabled' => $numbersDisabled
                    ])
                ;
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Design::class,
        ]);
    }
}
