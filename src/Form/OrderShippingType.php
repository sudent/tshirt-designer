<?php

namespace App\Form;

use App\Entity\OrderShipping;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderShippingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('recipientName', null, ['label' => 'Recipient Name'])
            ->add('recipientPhoneNumber', null, ['label' => 'Recipient Phone Number'])
            ->add('recipientEmailAddress', EmailType::class, ['label' => 'Recipient Email Address'])
            ->add('addressLine1', null, ['label' => 'Address Line 1'])
            ->add('addressLine2', null, ['label' => 'Address Line 2'])
            ->add('addressPostcode', null, ['label' => 'Postcode'])
            ->add('addressCity', null, ['label' => 'City'])
            ->add('addressState', null, ['label' => 'State'])
            ->add('shippingMethod', ChoiceType::class, [
                'label' => 'Preferred Shipping Method',
                'choices' => [
                    'PosLaju' => 'poslaju',
                    'J&T Express' => 'jnt',
                    'DHL' => 'dhl'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OrderShipping::class,
        ]);
    }
}
