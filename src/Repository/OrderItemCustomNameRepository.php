<?php

namespace App\Repository;

use App\Entity\OrderItemCustomName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderItemCustomName|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderItemCustomName|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderItemCustomName[]    findAll()
 * @method OrderItemCustomName[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderItemCustomNameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderItemCustomName::class);
    }

    // /**
    //  * @return OrderItemCustomName[] Returns an array of OrderItemCustomName objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderItemCustomName
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
