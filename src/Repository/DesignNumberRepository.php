<?php

namespace App\Repository;

use App\Entity\DesignNumber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DesignNumber|null find($id, $lockMode = null, $lockVersion = null)
 * @method DesignNumber|null findOneBy(array $criteria, array $orderBy = null)
 * @method DesignNumber[]    findAll()
 * @method DesignNumber[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DesignNumberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DesignNumber::class);
    }

    // /**
    //  * @return DesignNumber[] Returns an array of DesignNumber objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DesignNumber
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
