<?php

namespace App\Repository;

use App\Entity\CustomImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CustomImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomImage[]    findAll()
 * @method CustomImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomImage::class);
    }

    // /**
    //  * @return CustomImage[] Returns an array of CustomImage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CustomImage
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
