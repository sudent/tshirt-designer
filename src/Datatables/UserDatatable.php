<?php

namespace App\Datatables;

use App\Entity\User;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Filter\Select2Filter;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;

/**
* Class UserDatatable
* @package App\Datatables
*/
class UserDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = [])
    {
        $this->language->set([
            'cdn_language_by_locale' => true
        ]);

        $this->ajax->set([
        ]);

        $this->options->set([
            'classes' => Style::BOOTSTRAP_4_STYLE,
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'search_delay' => 3000,
            'order' => [
                [0, 'desc']
            ],
        ]);

        $this->features->set([
        ]);

        $this->columnBuilder
            ->add('createdAt', DateTimeColumn::class, [
                'title' => 'Registered Date',
            ])
            ->add('name', Column::class, [
                'title' => 'Name',
            ])
            ->add('email', Column::class, [
                'title' => 'Email',
                ])
            ->add('roles', Column::class, [
                'title' => 'Role'
                ])
            ->add('enabled', Column::class, [
                'title' => 'Status',
                'width' => '120px',
                'filter' => array(Select2Filter::class, array(
                    'placeholder' => 'Status',
                    'search_type' => 'eq',
                    'allow_clear' => true,
                    'select_options' => array(
                        '' => '',
                        true => 'Enabled',
                        false => 'Disabled',
                    ),
                    'classes' => 'dt-select2'
                )),
            ])
            ->add(null, ActionColumn::class, [
                'title' => $this->translator->trans('sg.datatables.actions.title'),
                'class_name' => 'ui-w-150',
                'actions' => [
                    [
                        'route' => 'admin_user_show',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        'label' => $this->translator->trans('sg.datatables.actions.show'),
                        'icon' => 'fad fa-eye',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.show'),
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'button'
                        ],
                    ],
                    [
                        'route' => 'admin_user_edit',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        'label' => $this->translator->trans('sg.datatables.actions.edit'),
                        'icon' => 'fa fa-edit',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'button'
                        ],
                    ]
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\User';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $formatter = function($line) {
            if (count($line['roles']) === 0){
                $line['roles'] = 'User';
            } else {
                $role = $line['roles'][0];

                if ($role === User::ROLE_ADMIN) {
                    $role = 'Admin';
                } elseif ($role === User::ROLE_USER) {
                    $role = 'User';
                }

                $line['roles'] = $role;
            }

            $badge = $line['enabled'] ? 'success' : 'danger';

            $line['enabled'] = "<span class='badge badge-${badge}'>"
                . ($line['enabled'] ? 'Enabled' : 'Disabled')
                . '</span>';

            return $line;
        };

        return $formatter;
    }
}
