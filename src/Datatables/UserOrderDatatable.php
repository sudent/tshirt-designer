<?php

namespace App\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;

class UserOrderDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = [])
    {
        $this->language->set([
            'cdn_language_by_locale' => true
        ]);

        $this->ajax->set([
        ]);

        $this->options->set([
            'classes' => Style::BOOTSTRAP_4_STYLE,
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'search_delay' => 3000,
            'order' => [
                [0, 'desc']
            ],
        ]);

        $this->features->set([
        ]);

        $this->columnBuilder
            ->add('createdAt', DateTimeColumn::class, [
                'title' => 'Order Date',
            ])
            ->add('number', Column::class, [
                'title' => 'Order Number',
            ])
            ->add('total', Column::class, [
                'title' => 'Total Amount',
            ])
            ->add('state', Column::class, [
                'title' => 'Status',
            ])
            ->add('updatedAt', DateTimeColumn::class, [
                'title' => 'Last Updated',
            ])
            ->add('uuid', Column::class, [
                'sent_in_response' => false,
                'searchable' => false,
            ])
            ->add(null, ActionColumn::class, [
                'title' => $this->translator->trans('sg.datatables.actions.title'),
                'class_name' => 'ui-w-150',
                'actions' => [
                    [
                        'route' => 'frontend_order_receipt',
                        'route_parameters' => [
                            'uuid' => 'uuid'
                        ],
                        'label' => $this->translator->trans('sg.datatables.actions.show'),
                        'icon' => 'fad fa-eye',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.show'),
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'button'
                        ],
                    ]
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\Order';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'order';
    }
}
