<?php

namespace App\Entity;

use App\Repository\DesignNumberRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DesignNumberRepository::class)
 */
class DesignNumber extends DesignData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Design::class, inversedBy="designNumbers")
     */
    private $design;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesign(): ?Design
    {
        return $this->design;
    }

    public function setDesign(?Design $design): self
    {
        $this->design = $design;

        return $this;
    }
}
