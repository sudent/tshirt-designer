<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use App\Repository\OrderItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderItemRepository::class)
 */
class OrderItem
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity = 1;

    /**
     * @ORM\Column(type="integer")
     */
    private $unitPrice = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $total = 0;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderItems")
     */
    private $orderParent;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=Design::class)
     */
    private $design;

    /**
     * @ORM\OneToMany(targetEntity=OrderItemCustomName::class, mappedBy="orderItem", cascade={"persist"})
     */
    private $orderItemCustomNames;

    /**
     * @ORM\Column(type="integer")
     */
    private $customNameTotal = 0;

    /**
     * @ORM\OneToMany(targetEntity=OrderItemVariant::class, mappedBy="orderItem", cascade={"persist"})
     */
    private $orderItemVariants;

    public function __construct()
    {
        $this->orderItemCustomNames = new ArrayCollection();
        $this->orderItemVariants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getUnitPrice(): ?int
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(int $unitPrice): self
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getOrderParent(): ?Order
    {
        return $this->orderParent;
    }

    public function setOrderParent(?Order $orderParent): self
    {
        $this->orderParent = $orderParent;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getDesign(): ?Design
    {
        return $this->design;
    }

    public function setDesign(?Design $design): self
    {
        $this->design = $design;

        return $this;
    }

    /**
     * @return Collection|OrderItemCustomName[]
     */
    public function getOrderItemCustomNames(): Collection
    {
        return $this->orderItemCustomNames;
    }

    public function addOrderItemCustomName(OrderItemCustomName $orderItemCustomName): self
    {
        if (!$this->orderItemCustomNames->contains($orderItemCustomName)) {
            $this->orderItemCustomNames[] = $orderItemCustomName;
            $orderItemCustomName->setOrderItem($this);
        }

        return $this;
    }

    public function removeOrderItemCustomName(OrderItemCustomName $orderItemCustomName): self
    {
        if ($this->orderItemCustomNames->removeElement($orderItemCustomName)) {
            // set the owning side to null (unless already changed)
            if ($orderItemCustomName->getOrderItem() === $this) {
                $orderItemCustomName->setOrderItem(null);
            }
        }

        return $this;
    }

    public function getCustomNameTotal(): ?int
    {
        return $this->customNameTotal;
    }

    public function setCustomNameTotal(int $customNameTotal): self
    {
        $this->customNameTotal = $customNameTotal;

        return $this;
    }

    /**
     * @return Collection|OrderItemVariant[]
     */
    public function getOrderItemVariants(): Collection
    {
        return $this->orderItemVariants;
    }

    public function addOrderItemVariant(OrderItemVariant $orderItemVariant): self
    {
        if (!$this->orderItemVariants->contains($orderItemVariant)) {
            $this->orderItemVariants[] = $orderItemVariant;
            $orderItemVariant->setOrderItem($this);
            $orderItemVariant->setUnitPrice($this->getUnitPrice());
        }

        return $this;
    }

    public function removeOrderItemVariant(OrderItemVariant $orderItemVariant): self
    {
        if ($this->orderItemVariants->removeElement($orderItemVariant)) {
            // set the owning side to null (unless already changed)
            if ($orderItemVariant->getOrderItem() === $this) {
                $orderItemVariant->setOrderItem(null);
            }
        }

        return $this;
    }

    public function recalculateTotal()
    {
        $total = 0;
        $quantity = 0;

        foreach ($this->getOrderItemVariants() as $orderItemVariant) {
            $orderItemVariant->recalculateTotal();

            $total += $orderItemVariant->getTotal();
            $quantity += $orderItemVariant->getQuantity();
        }

        $total += 500 * $this->getOrderItemCustomNames()->count();

        $this->setTotal($total);
        $this->setQuantity($quantity);
    }
}
