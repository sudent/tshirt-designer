<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use App\Repository\DesignRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=DesignRepository::class)
 * @UniqueEntity(fields={"uuid"}, message="System error. Please notify administrator.")
 */
class Design
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="designs")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $backgroundColor;

    /**
     * @ORM\Column(type="json")
     */
    private $frontSideJsonData = [];

    /**
     * @ORM\Column(type="json")
     */
    private $backSideJsonData = [];

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private $uuid;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasNames = false;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $namesSide;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $namesHeight;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $namesColor;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasNumbers = false;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $numbersSide;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numbersHeight;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $numbersColor;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $frontSideThumbnail;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $backSideThumbnail;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2, options={"default"=530})
     */
    private $originalWidth;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2, options={"default"=630})
     */
    private $originalHeight;

    public function __construct()
    {
        $this->uuid = Uuid::v4();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }

    public function setBackgroundColor(?string $backgroundColor): self
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    public function getFrontSideJsonData(): ?array
    {
        return $this->frontSideJsonData;
    }

    public function setFrontSideJsonData(array $frontSideJsonData): self
    {
        $this->frontSideJsonData = $frontSideJsonData;

        return $this;
    }

    public function getBackSideJsonData(): ?array
    {
        return $this->backSideJsonData;
    }

    public function setBackSideJsonData(array $backSideJsonData): self
    {
        $this->backSideJsonData = $backSideJsonData;

        return $this;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid($uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getHasNames(): ?bool
    {
        return $this->hasNames;
    }

    public function setHasNames(bool $hasNames): self
    {
        $this->hasNames = $hasNames;

        return $this;
    }

    public function getNamesSide(): ?string
    {
        return $this->namesSide;
    }

    public function setNamesSide(?string $namesSide): self
    {
        $this->namesSide = $namesSide;

        return $this;
    }

    public function getNamesHeight(): ?int
    {
        return $this->namesHeight;
    }

    public function setNamesHeight(?int $namesHeight): self
    {
        $this->namesHeight = $namesHeight;

        return $this;
    }

    public function getNamesColor(): ?string
    {
        return $this->namesColor;
    }

    public function setNamesColor(?string $namesColor): self
    {
        $this->namesColor = $namesColor;

        return $this;
    }

    public function getHasNumbers(): ?bool
    {
        return $this->hasNumbers;
    }

    public function setHasNumbers(bool $hasNumbers): self
    {
        $this->hasNumbers = $hasNumbers;

        return $this;
    }

    public function getNumbersSide(): ?string
    {
        return $this->numbersSide;
    }

    public function setNumbersSide(?string $numbersSide): self
    {
        $this->numbersSide = $numbersSide;

        return $this;
    }

    public function getNumbersHeight(): ?int
    {
        return $this->numbersHeight;
    }

    public function setNumbersHeight(?int $numbersHeight): self
    {
        $this->numbersHeight = $numbersHeight;

        return $this;
    }

    public function getNumbersColor(): ?string
    {
        return $this->numbersColor;
    }

    public function setNumbersColor(?string $numbersColor): self
    {
        $this->numbersColor = $numbersColor;

        return $this;
    }

    public function getFrontSideThumbnail()
    {
        return $this->frontSideThumbnail;
    }

    public function getFrontSideThumbnailContent()
    {
        return $this->getFrontSideThumbnail() ? stream_get_contents($this->getFrontSideThumbnail()) : null;
    }

    public function setFrontSideThumbnail($frontSideThumbnail): self
    {
        $this->frontSideThumbnail = $frontSideThumbnail;

        return $this;
    }

    public function getBackSideThumbnail()
    {
        return $this->backSideThumbnail;
    }

    public function getBackSideThumbnailContent()
    {
        return $this->getBackSideThumbnail() ? stream_get_contents($this->getBackSideThumbnail()) : null;
    }

    public function setBackSideThumbnail($backSideThumbnail): self
    {
        $this->backSideThumbnail = $backSideThumbnail;

        return $this;
    }

    public function getOriginalWidth(): ?string
    {
        return $this->originalWidth;
    }

    public function setOriginalWidth(string $originalWidth): self
    {
        $this->originalWidth = $originalWidth;

        return $this;
    }

    public function getOriginalHeight(): ?string
    {
        return $this->originalHeight;
    }

    public function setOriginalHeight(string $originalHeight): self
    {
        $this->originalHeight = $originalHeight;

        return $this;
    }

    public function __toString()
    {
        return $this->getUuid()->toRfc4122();
    }
}
