<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use App\Entity\Traits\ToggleableTrait;
use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @Vich\Uploadable()
 * @UniqueEntity(fields={"code"})
 */
class Product
{
    use TimestampableEntity;
    use ToggleableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="decimal", precision=12, scale=2)
     */
    private $basePrice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagePath;

    /**
     * @var File|null
     * @Assert\NotBlank(groups="new")
     * @Assert\NotNull(groups="new")
     * @Assert\Image(
     *     maxSize = "5M"
     * )
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="imagePath")
     */
    private $image;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeleteable = true;

    /**
     * @var File|null
     * @Assert\Image(
     *     maxSize = "5M"
     * )
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="frontSideImagePath")
     */
    private $frontSideImageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $frontSideImagePath;

    /**
     * @var File|null
     * @Assert\Image(
     *     maxSize = "5M"
     * )
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="backSideImagePath")
     */
    private $backSideImageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $backSideImagePath;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBasePrice(): ?string
    {
        return $this->basePrice;
    }

    public function setBasePrice(string $basePrice): self
    {
        $this->basePrice = $basePrice;

        return $this;
    }

    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    public function setImagePath(?string $imagePath): self
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImage(): ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setImage(?File $image): void
    {
        $this->image = $image;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getIsDeleteable(): ?bool
    {
        return $this->isDeleteable;
    }

    public function setIsDeleteable(bool $isDeleteable): self
    {
        $this->isDeleteable = $isDeleteable;

        return $this;
    }

    public function getFrontSideImagePath(): ?string
    {
        return $this->frontSideImagePath;
    }

    public function setFrontSideImagePath(?string $frontSideImagePath): self
    {
        $this->frontSideImagePath = $frontSideImagePath;

        return $this;
    }

    public function getBackSideImagePath(): ?string
    {
        return $this->backSideImagePath;
    }

    public function setBackSideImagePath(?string $backSideImagePath): self
    {
        $this->backSideImagePath = $backSideImagePath;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getFrontSideImageFile(): ?File
    {
        return $this->frontSideImageFile;
    }

    /**
     * @param File|null $frontSideImageFile
     */
    public function setFrontSideImageFile(?File $frontSideImageFile): void
    {
        $this->frontSideImageFile = $frontSideImageFile;
    }

    /**
     * @return File|null
     */
    public function getBackSideImageFile(): ?File
    {
        return $this->backSideImageFile;
    }

    /**
     * @param File|null $backSideImageFile
     */
    public function setBackSideImageFile(?File $backSideImageFile): void
    {
        $this->backSideImageFile = $backSideImageFile;
    }
}
