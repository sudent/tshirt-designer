<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use App\Repository\OrderShippingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderShippingRepository::class)
 */
class OrderShipping
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $recipientName;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $recipientPhoneNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $recipientEmailAddress;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $addressLine1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $addressLine2;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $addressPostcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $addressCity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $addressState;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shippingMethod = 'poslaju';

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state = 'new';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecipientName(): ?string
    {
        return $this->recipientName;
    }

    public function setRecipientName(string $recipientName): self
    {
        $this->recipientName = $recipientName;

        return $this;
    }

    public function getRecipientPhoneNumber(): ?string
    {
        return $this->recipientPhoneNumber;
    }

    public function setRecipientPhoneNumber(string $recipientPhoneNumber): self
    {
        $this->recipientPhoneNumber = $recipientPhoneNumber;

        return $this;
    }

    public function getRecipientEmailAddress(): ?string
    {
        return $this->recipientEmailAddress;
    }

    public function setRecipientEmailAddress(string $recipientEmailAddress): self
    {
        $this->recipientEmailAddress = $recipientEmailAddress;

        return $this;
    }

    public function getAddressLine1(): ?string
    {
        return $this->addressLine1;
    }

    public function setAddressLine1(string $addressLine1): self
    {
        $this->addressLine1 = $addressLine1;

        return $this;
    }

    public function getAddressLine2(): ?string
    {
        return $this->addressLine2;
    }

    public function setAddressLine2(?string $addressLine2): self
    {
        $this->addressLine2 = $addressLine2;

        return $this;
    }

    public function getAddressPostcode(): ?string
    {
        return $this->addressPostcode;
    }

    public function setAddressPostcode(string $addressPostcode): self
    {
        $this->addressPostcode = $addressPostcode;

        return $this;
    }

    public function getAddressCity(): ?string
    {
        return $this->addressCity;
    }

    public function setAddressCity(string $addressCity): self
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    public function getAddressState(): ?string
    {
        return $this->addressState;
    }

    public function setAddressState(string $addressState): self
    {
        $this->addressState = $addressState;

        return $this;
    }

    public function getShippingMethod(): ?string
    {
        return $this->shippingMethod;
    }

    public function setShippingMethod(string $shippingMethod): self
    {
        $this->shippingMethod = $shippingMethod;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }
}
