<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 * @UniqueEntity(fields={"uuid"}, message="System error. Please notify administrator.")
 */
class Order
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $state = 'new';

    /**
     * @ORM\Column(type="integer")
     */
    private $itemsTotal = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $shippingTotal = 1000;

    /**
     * @ORM\Column(type="integer")
     */
    private $total = 0;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=OrderItem::class, mappedBy="orderParent", cascade={"persist"})
     */
    private $orderItems;

    /**
     * @ORM\OneToMany(targetEntity=OrderPayment::class, mappedBy="orderParent", cascade={"persist"})
     */
    private $orderPayments;

    /**
     * @ORM\OneToOne(targetEntity=OrderShipping::class, cascade={"persist", "remove"})
     */
    private $orderShipping;

    /**
     * @ORM\OneToOne(targetEntity=OrderPayment::class, cascade={"persist", "remove"})
     */
    private $paidOrderPayment;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
        $this->orderPayments = new ArrayCollection();
        $this->uuid = Uuid::v4();
        $this->orderShipping = new OrderShipping();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid($uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getItemsTotal(): ?int
    {
        return $this->itemsTotal;
    }

    public function setItemsTotal(int $itemsTotal): self
    {
        $this->itemsTotal = $itemsTotal;

        return $this;
    }

    public function getShippingTotal(): ?int
    {
        return $this->shippingTotal;
    }

    public function setShippingTotal(int $shippingTotal): self
    {
        $this->shippingTotal = $shippingTotal;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setOrderParent($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): self
    {
        if ($this->orderItems->removeElement($orderItem)) {
            // set the owning side to null (unless already changed)
            if ($orderItem->getOrderParent() === $this) {
                $orderItem->setOrderParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrderPayment[]
     */
    public function getOrderPayments(): Collection
    {
        return $this->orderPayments;
    }

    public function addOrderPayment(OrderPayment $orderPayment): self
    {
        if (!$this->orderPayments->contains($orderPayment)) {
            $this->orderPayments[] = $orderPayment;
            $orderPayment->setOrderParent($this);
        }

        return $this;
    }

    public function removeOrderPayment(OrderPayment $orderPayment): self
    {
        if ($this->orderPayments->removeElement($orderPayment)) {
            // set the owning side to null (unless already changed)
            if ($orderPayment->getOrderParent() === $this) {
                $orderPayment->setOrderParent(null);
            }
        }

        return $this;
    }

    public function getOrderShipping(): ?OrderShipping
    {
        return $this->orderShipping;
    }

    public function setOrderShipping(?OrderShipping $orderShipping): self
    {
        $this->orderShipping = $orderShipping;

        return $this;
    }

    public function recalculateTotal()
    {
        $total = 0;

        foreach ($this->getOrderItems() as $orderItem) {
            $orderItem->recalculateTotal();

            $total += $orderItem->getTotal();
        }

        $this->setItemsTotal($total);

        $total += $this->getShippingTotal();

        $this->setTotal($total);
    }

    public function getPaidOrderPayment(): ?OrderPayment
    {
        return $this->paidOrderPayment;
    }

    public function setPaidOrderPayment(?OrderPayment $paidOrderPayment): self
    {
        $this->paidOrderPayment = $paidOrderPayment;

        return $this;
    }

    public function __toString()
    {
        return '#' . $this->getNumber();
    }
}
