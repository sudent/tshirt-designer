<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait ArchivalTrait
{
    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $isArchived = false;

    /**
     * @return bool
     */
    public function isArchived(): bool
    {
        return $this->isArchived;
    }

    /**
     * @param bool $isArchived
     * @return self
     */
    public function setIsArchived(bool $isArchived): self
    {
        $this->isArchived = $isArchived;
        return $this;
    }
}
