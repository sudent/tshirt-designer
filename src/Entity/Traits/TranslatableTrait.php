<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use Gedmo\Mapping\Annotation as Gedmo;

trait TranslatableTrait
{
    /**
     * @Gedmo\Locale
     */
    private $locale = 'ms';

    /**
     * Set the locale to use for translation listener
     *
     * @param string $locale
     *
     * @return static
     */
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }
}
