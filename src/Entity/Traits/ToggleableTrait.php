<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait ToggleableTrait
{
    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true, options={"default"=true})
     */
    protected $enabled = true;

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $isEnabled
     */
    public function setEnabled(?bool $isEnabled): void
    {
        $this->enabled = (bool) $isEnabled;
    }

    public function enable(): void
    {
        $this->enabled = true;
    }

    public function disable(): void
    {
        $this->enabled = false;
    }
}
